//-----------------------------------------------------------------------
// clock.c
//
// Zeitfunktionen und Takteinstellung
//  - Zeitfunktionen (ca.-Funktionen, Taktabh�ngig!)
//  - Externen Clock ein- bzw. abschalten
//
//-----------------------------------------------------------------------
//#include  <msp430x26x.h>
#include <msp430.h>
#include "main_common.h"

void initClock(void) // ProzessorClocks setzen (externer Quarz)
{
	defaultClock();
}

// Normaler Arbeitsmodus
// ACLK =12kHz
// MCLK =7.3728MHz
// SMCLK=7.3728MHz
void onXT2Clock(void) {
	if (!checkPower(NORMAL))
		exitLowBat();

	UCSCTL0 = 0x0000; // DCO_0 + MOD_0;
					  // 0x0000;     // muss noch gepr�ft werden, DCO tap selection, Einstellung noch zu kl�ren, vorher nicht vorhanden
	// DCO_0 + MOD_0;

	UCSCTL1 = DCORSEL_3 + // DCORSEL + DISMOD, DCO frequency range select, *DCO_3*,
			DISMOD; // DISMOD off (Modulation disabled, vorher mit MOD_0), muss noch gepr�ft werden

	UCSCTL2 = 0x0000; // divider of FFL feedbackloop (1) vorher nicht vorhanden,
					  // FLLD_0 + FLLN_0;    // multiplier of FFL feedbackloop (1) vorher nicht vorhanden, noch abzukl�ren

	UCSCTL3 = SELREF_5 + // XT2CLK when available, otherwise REFOCLK,noch zu kl�ren,
			FLLREFDIV_0; // FLL Teiler auf 1

	// source ACLK SMCLK & MCLK w�hlen keine �nderung zu default clock
	UCSCTL4 = SELA_5 + // ACLK = VLOCLK;
			SELS_5 + // SMCLK = DCOCLK;
			SELM_5;  // MCLK = DCOCLK

	// source divider ACLK SMCLK & MCLK w�hlenkeine �nderung zu default clock
	UCSCTL5 = DIVPA_0 + // alle Teiler auf 1
			DIVA_0 + DIVS_0 + DIVM_0;

	UCSCTL6 |= XT2DRIVE_0 + XCAP_0;
	/*
	 XT2DRIVE_0 +  // XT2DRIVE = 00 (einstellung oscillator strom, vorher nicht vorhanden, 00 f�r 4 bis 8 MHz)
	 // XT2BYPASS = 0 (XT2 sourced from external crystal, vorher nicht vorhanden)
	 //XT2OFF +      // XT2OFF = 0 (XT2 is ON wenn PORTfunktion freigegeben es nicht f�r SMCLK und MCLK genutzt wird)
	 XT1DRIVE_0 +   // XT1DRIVE = 00 (einstellung oscillator strom, vorher nicht vorhanden, 00 f�r 4 bis 8 MHz)
	 // XTS = 0 (low frequency mode at XT1)
	 // XT1BYPASS = 0 (XT1 sourced from external crystal, vorher nicht vorhanden)
	 XCAP_0;         // XCAP = 0 (1p)
	 // SMCLKOFF = 0 (SMCLK is on, vorher nicht vorhanden)
	 // XT1OFF = 0 (on wenn �ber Port selection aktiviert, vorher nicht vorhanden)
	 */
	//       folgendes Flag setzen und abfragen arbeitet nicht!!!
	uint16_t osc_fault = false;
	do {

		UCSCTL7 &= ~(XT1LFOFFG + DCOFFG + XT2OFFG);
		volatile uint16_t UCSCTL7_var = UCSCTL7;
		osc_fault = UCSCTL7_var & XT2OFFG;
		// UCSCTL7 = 0;
		// UCSCTL7_var = UCSCTL7;
		// XT2 oscillator fault flag. If this bit is set, the OFIFG flag is also set. XT2OFFG is
		// set if a XT2 fault condition exists. XT2OFFG can be cleared by software. If the
		// XT2 fault condition still remains, XT2OFFG is set.
		// 0b = No fault condition occurred after the last reset.
		// 1b = XT2 fault. An XT2 fault occurred after the last reset.
		// SFRIFG1 &= ~OFIFG;
		_delay(5000);
	}

	while (osc_fault); // wait, while OFIFG is set
	_delay(1024 / 7);  // mind. 1024 Clock-Zyklen abwarten
	SFRIFG1 &= ~OFIFG;
	// MCLK source is XT2, MCLK div is 1
	// SMCLK source is XT2, SMCLK div is 1

	// BCSCTL2 |= SELM_2 + DIVM_0 + SELS + DIVS_0;
	// source ACLK SMCLK & MCLK auf XT2 setzen
	UCSCTL4 |= SELA_5 + // ACLK = XT2;
			SELS_5 + // SMCLK = XT2;
			SELM_5;  // MCLK = XT2

	UCSCTL5 = DIVPA_0 + // alle Teiler auf 1
			DIVA_0 + DIVS_0 + DIVM_0;
}

void offXT2Clock(void) {
	defaultClock();
}

// LowPower-Modus
// ACLK =12kHz
// MCLK =12kHz
// SMCLK=1MHz (wenn noch vorhanden)
void LPMClock(void) {

	UCSCTL0 = 0x0000; // muss noch gepr�ft werden, DCO tap selection, Einstellung noch zu kl�ren, vorher nicht vorhanden
					  // DCO_0 + MOD_0;

	UCSCTL1 = DCORSEL_3 + // DCORSEL + DISMOD, DCO frequency range select, *DCO_3*,
			DISMOD; // DISMOD off (Modulation disabled, vorher mit MOD_0), muss noch gepr�ft werden

	UCSCTL2 = 0x0000; // divider of FFL feedbackloop (1) vorher nicht vorhanden,
					  // FLLD_0 + FLLN_0;    // multiplier of FFL feedbackloop (1) vorher nicht vorhanden, noch abzukl�ren

	// FLL reference select & divider, vorher nicht vorhanden, noch zu kl�ren
	UCSCTL3 = SELREF_5 + // XT2CLK when available, otherwise REFOCLK,noch zu kl�ren,
			FLLREFDIV_0; // FLL Teiler auf 1

	// source ACLK SMCLK & MCLK w�hlen
	UCSCTL4 = SELA_1 + // ACLK = VLOCLK;
			SELS_1 + // SMCLK = VLOCLK  // eigentlich SELS_3 (SMCLK = DCOCLK) aus alter Firmware;
			SELM_1;  // MCLK = VLOCLK

	/*
	 // source divider ACLK SMCLK & MCLK w�hlen
	 UCSCTL5 = DIVPA_0 + // alle Teiler auf 1
	 DIVA_0 +
	 DIVS_0 +
	 DIVM_0;




	 UCSCTL6 = XT2DRIVE_0 +  // XT2DRIVE = 00 (einstellung oscillator strom, vorher nicht vorhanden, 00 f�r 4 bis 8 MHz), alles andere ist h�her
	 // XT2BYPASS = 0 (XT2 sourced from external crystal, vorher nicht vorhanden)
	 XT2OFF +      // XT2OFF = 1 (XT2 is OFF wenn es nicht f�r SMCLK und MCLK genutzt wird)
	 XT1DRIVE_0 +   // XT1DRIVE = 00 (einstellung oscillator strom, vorher nicht vorhanden, 00 f�r 4 bis 8 MHz)
	 // XTS = 0 (low frequency mode at XT1)
	 // XT1BYPASS = 0 (XT1 sourced from external crystal, vorher nicht vorhanden)
	 XCAP_0;         // XCAP = 0 (1p)
	 // SMCLKOFF = 0 (SMCLK is on, vorher nicht vorhanden)
	 // XT1OFF = 0 (on wenn �ber Port selection aktiviert, vorher nicht vorhanden)

	 UCSCTL7 = 0x0000; // Oscillatorenfehlerflags, vorher nicht vorhanden

	 UCSCTL8 = 0x0000; // MODOSC, SMCLK, MCLK, ACLK clock request enable, all disabled, vorher nicht vorhanden

	 */
}

// Normaler Arbeitsmodus
// ACLK =12kHz
// MCLK =1MHz
// SMCLK=1MHz
void defaultClock(void) {

	// nach Datenblatt ist alte Frequenz bei etwa 1MHz; dies wird mit folgender Einstellung nach Datenblatt erreicht: DCORSELx = 3, DCOx = 0, MODx = 0
	UCSCTL0 = 0x0000; // muss noch gepr�ft werden, DCO tap selection, Einstellung noch zu kl�ren, vorher nicht vorhanden
					  // DCO_0 + MOD_0;

	UCSCTL1 = DCORSEL_3 + // DCORSEL + DISMOD, DCO frequency range select, *DCO_3*,
			DISMOD; // DISMOD off (Modulation disabled, vorher mit MOD_0), muss noch gepr�ft werden

	UCSCTL2 = 0x0000; // divider of FFL feedbackloop (1) vorher nicht vorhanden,
					  // FLLD_0 + FLLN_0;    // multiplier of FFL feedbackloop (1) vorher nicht vorhanden, noch abzukl�ren

	// FLL reference select & divider, vorher nicht vorhanden, noch zu kl�ren
	UCSCTL3 = SELREF_2 + // XT2CLK when available, otherwise REFOCLK,noch zu kl�ren,
			FLLREFDIV_0; // FLL Teiler auf 1

	// source ACLK SMCLK & MCLK w�hlen
	UCSCTL4 = SELA_1 + // ACLK = VLOCLK;
			SELS_3 + // SMCLK = DCOCLK;
			SELM_3;  // MCLK = DCOCLK

	// source divider ACLK SMCLK & MCLK w�hlen
	UCSCTL5 = DIVPA_0 + // alle Teiler auf 1
			DIVA_0 + DIVS_0 + DIVM_0;

	UCSCTL6 = XT2DRIVE_0 + // XT2DRIVE = 00 (einstellung oscillator strom, vorher nicht vorhanden, 00 f�r 4 bis 8 MHz)
							// XT2BYPASS = 0 (XT2 sourced from external crystal, vorher nicht vorhanden)
			XT2OFF + // XT2OFF = 1 (XT2 is OFF wenn es nicht f�r SMCLK und MCLK genutzt wird)
			XT1DRIVE_0 + // XT1DRIVE = 00 (einstellung oscillator strom, vorher nicht vorhanden, 00 f�r 4 bis 8 MHz)
						  // XTS = 0 (low frequency mode at XT1)
						  // XT1BYPASS = 0 (XT1 sourced from external crystal, vorher nicht vorhanden)
			XCAP_0 +     // XCAP = 0 (1p)
						  // SMCLKOFF = 0 (SMCLK is on, vorher nicht vorhanden)
			XT1OFF; // XT1OFF = 1 (on wenn �ber Port selection aktiviert, vorher nicht vorhanden)

	UCSCTL7 = 0x0000; // Oscillatorenfehlerflags, vorher nicht vorhanden

	UCSCTL8 = 0x0000; // MODOSC, SMCLK, MCLK, ACLK clock request enable, all disabled, vorher nicht vorhanden
}
