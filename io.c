//-----------------------------------------------------------------------
// io.c
//
// IO - Allgemeine In-/Outputs
//  - LEDs
//  - Schalter
//  - Jumper
//  - Shutdown-Signale
//
//-----------------------------------------------------------------------
#include "main_common.h"

// Ports einstellen
void initIO(void) {
	P1DIR = 0x8F; // P1 out, P1.4/P1.5/P1.6 in
	P1SEL = 0x00; // P1 I/O Functions
	// P1SEL2 = 0x00;
	P1OUT = 0x00;  // P1 off
	P1REN = 0x00;  // P1 kein Pu/Pd Resistor
	P1OUT |= 0x80; // P1.7 off, /Shutdown Probe off

	P2DIR = 0xDB; // P2 out, P2.2/P2.5 in
	P2SEL = 0x00; // P2 I/O Functions
	// P2SEL2 = 0x00;
	P2OUT = 0x00; // P2 off
	P2REN = 0x00; // P2 kein Pu/Pd Resistor

	P3DIR = 0xEF; // P3 out, P3.4 in
	P3SEL = 0x00; // P3 I/O Functions
	// P3SEL2 = 0x00;
	P3OUT = 0x00; // P3 off
	P3REN = 0x00; // P3 kein Pu/Pd Resistor

	P4DIR = 0x1E; // P4 out, P4.0/P4.4/P4.5/P4.6/P4.7 in
	P4SEL = 0x00; // P4 I/O Functions
	// P4SEL2 = 0x00;
	P4OUT = 0x00;   // P4 off
	P4REN = 0x00;   // P4 kein Pu/Pd Resistor
	P4OUT &= ~0x08; // P4.3 off, Bluetooth off (nur zum sichergehen)

	// Hier sind Sonderfunktionen zu starten die beim 2619 extra Pins hatten
	P5DIR = 0xE0; // P5 out, P5.0/P5.1/P5.2/P5.3/P5.4 in
	P5SEL = 0x0F; // P5 I/O Functions, XT2 und Vref-Funktion frei
	// P5SEL2 = 0x0F;
	P5OUT = 0x00; // P5 off
	P5REN = 0x00; // P5 kein Pu/Pd Resistor

	P6DIR = 0xE0; // P6 out P6.0/P6.1/P6.2/P6.3/P6.4 in
	P6SEL = 0x00; // P6 I/O Functions
	// P6SEL2 = 0x00;
	P6OUT = 0x00; // P6 off
	P6REN = 0x00; // P6 kein Pu/Pd Resistor
}

// Programmende mit Low-Bat-Anzeige und Sonde an
void exitLowBat(void) {
	onLowBatLED();     // P3.0 on, LowBat an
	offStateLED();     // P3.1 off, Status aus
	onShutdownProbe(); // P5.3 on,  /Shutdown Probe on
	offBluetooth();    // P5.4 off, Bluetooth off
	while (1) {
	}
}
