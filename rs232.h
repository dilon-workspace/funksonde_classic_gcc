/*
 * rs232.h
 *
 *  Created on: 09.01.2020
 *      Author: js
 */

#ifndef RS232_H_
#define RS232_H_

typedef enum {
	uart_com0, uart_com1, uart_MAX
} uart_com_index_t;

// rs232.c
void initRS232(void); // RS232 initialisieren
void onRS232(void);   //. einschalten
void offRS232(void);  //. abschalten
// Daten senden
void sendRS232(uart_com_index_t ucPort, unsigned char *pBuffer,
		unsigned short wSize);
// Daten empfangen
unsigned short receiveRS232(uart_com_index_t ucPort, unsigned char *pBuffer,
		unsigned short wSize);
// ein Byte empfangen
bool receiveByteRS232(uart_com_index_t ucPort, unsigned char *puc);
#endif /* RS232_H_ */
