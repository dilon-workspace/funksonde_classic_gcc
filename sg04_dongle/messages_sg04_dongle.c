/*
 * messages_sg04_dongle.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include <string.h>
#include <stdio.h>

#include "../messages_common.h"
#include "../main_common.h"
#include "../info.h"
#include "../sg04/soft_uart_tx_sg04.h"

char g_Type2[] = "Empfangsmodul\r\n"
		"----------------------------------------------------\r\n";

void parse_received_message(strMESSAGE g_stMsgRec) {
	// Messages f�r Empf�nger
	uint16_t w = 0;
	// MT101018 Zus�tzlicher Befehl
	if (g_stMsgRec.ucType == MSG_CMDERR) {
		Data_send_null_terminated_string(uart_com1, "!ERR\r\n");
	}
	// MT101018 Zus�tzlicher Befehl
	if (g_stMsgRec.ucType == MSG_CMDOK) {
		Data_send_null_terminated_string(uart_com1, "!OK\r\n");
	}
	// Messdaten Ticks
	if (g_stMsgRec.ucType == MSG_TICKS) { // ob zweiter Kanal ist noch zu pr�fen
		if (g_ucModus == board_mode_reeiver_pc) {
			uint16_t value1 = 0;
			uint16_t value2 = 0;
			const size_t BUFFER_LENGTH = 100;
			char g_ucBuffer[BUFFER_LENGTH]; // Zwischenbuffer f�r DataReceivedCheck
			memcpy(&value1, &g_stMsgRec.ucData[0], sizeof(uint16_t));
			memcpy(&value2, &g_stMsgRec.ucData[2], sizeof(uint16_t));

			// hier mu� der zweite Wert sp�ter ausgelesen und verarbeitet werden!!!
			snprintf(g_ucBuffer, BUFFER_LENGTH, "!MTI%u\r%u\r\n", value1,
					value2);
			Data_send_null_terminated_string(uart_com1, g_ucBuffer);
		} else {

			//######################################################################################
			// 27.08.2010, RL
			// Verz�gerung Ausgabe UD-OUT bei Nuklidwechsel
			//######################################################################################
			if (g_wNukl > 0 && g_bNuklNoChangeFlag) {
				uint16_t value1 = 0;
				uint16_t value2 = 0;
				memcpy(&value1, &g_stMsgRec.ucData[0], sizeof(uint16_t));
				//Generieren der Pulsfolge;
				writeTimerTicksToFifoA1(value1);
				memcpy(&value2, &g_stMsgRec.ucData[2], sizeof(uint16_t));
				//versenden der zweiten Pulsfolge als Datenwort

				soft_uart_tx_send_dual_nuklid_ticks(value2);
				//writeTimerTicksToFifoA2(value2);
			}
		}
	}

	// Versionsnummer lesen
	if (g_stMsgRec.ucType == MSG_GETVERSION) {
		const size_t BUFFER_LENGTH = 100;
		char g_ucBuffer[BUFFER_LENGTH]; // Zwischenbuffer f�r DataReceivedCheck
		snprintf(g_ucBuffer, BUFFER_LENGTH, "!#GV%s\r\n", g_stMsgRec.ucData);
		Data_send_null_terminated_string(uart_com1, g_ucBuffer);
	}
	// Seriennummer lesen
	if (g_stMsgRec.ucType == MSG_GETSERIAL) {
		const size_t BUFFER_LENGTH = 100;
		char g_ucBuffer[BUFFER_LENGTH];
		snprintf(g_ucBuffer, BUFFER_LENGTH, "!#GS%s\r\n", g_stMsgRec.ucData);
		Data_send_null_terminated_string(uart_com1, g_ucBuffer);
	}
	// Sondenname lesen
	if (g_stMsgRec.ucType == MSG_GETNAME) {
		const size_t BUFFER_LENGTH = 100;
		char g_ucBuffer[BUFFER_LENGTH];
		snprintf(g_ucBuffer, BUFFER_LENGTH, "!GNM%s\r\n", g_stMsgRec.ucData);
		Data_send_null_terminated_string(uart_com1, g_ucBuffer);
	}
	// Nuklideintrag lesen
	if (g_stMsgRec.ucType == MSG_GETNUKLID) {
		memcpy(&w, &g_stMsgRec.ucData[0], sizeof(uint16_t));

		if (w > 0 && w < NUKLIDMAX) {
			const size_t BUFFER_LENGTH = 100;
			char g_ucBuffer[BUFFER_LENGTH];
			uint16_t value1 = 0;
			uint16_t value2 = 0;
			memcpy(&value1, &g_stMsgRec.ucData[2], sizeof(uint16_t));
			memcpy(&value2, &g_stMsgRec.ucData[4], sizeof(uint16_t));
			snprintf(g_ucBuffer, BUFFER_LENGTH, "!#GU%d,%d,%d\r\n", w, value1,
					value2);
			Data_send_null_terminated_string(uart_com1, g_ucBuffer);
		}
	}
	// Sondenstatus
	if (g_stMsgRec.ucType == MSG_PROBESTAT) {
		memcpy(&w, &g_stMsgRec.ucData[0], sizeof(uint16_t));
	}
}
