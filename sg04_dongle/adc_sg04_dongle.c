/*
 * adc_sg04_dongle.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include <msp430.h>
void adc_init_port(void) {
    P6DIR &= ~0x12; // P6.1/P6.4 als Input
    P6SEL |= 0x12;  // ADC-Function
    // P6SEL2 &= ~0x12; // - "" -
    P6REN &= ~0x12; // P6.1/P6.4/P6.5 ohne PullUpDown
}

void adc_init_sequence(void) {
    ADC12MCTL1 = ADC12INCH_1 + ADC12SREF_1;            //+5V
    ADC12MCTL4 = ADC12INCH_4 + ADC12SREF_1 + ADC12EOS; //+3.3V
    ADC12IE = 0x0012;
}
