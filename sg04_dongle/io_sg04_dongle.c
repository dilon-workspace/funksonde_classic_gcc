/*
 * io_sg04_dongle.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include <stdint.h>
#include "../main_common.h"

// Sonde einschalten
void powerOn(void) {
    uint16_t wMS = 0;
    bool bon = false;

    onShutdownProbe(); // P5.3 on, /Shutdown Probe on
    onBluetooth();     // P5.4 on, Bluetooth on
    onStateLED();      // P3.1 on, Status LED on

    Data_send_null_terminated_string(uart_com1, g_PowerOn);
}

// Sonde ausschalten
void powerOff(void) {
    Data_send_null_terminated_string(uart_com0, g_PowerOff);
    offLowBatLED();     // P2.7 off, LowBat aus
    offStateLED();      // P2.6 off, Status aus
    offShutdownProbe(); // P1.7 off, /Shutdown Probe off
    offBluetooth();     // P4.3 off, Bluetooth off
}
