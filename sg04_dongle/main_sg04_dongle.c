/*
 * main_sg04_dongle.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include <assert.h>
#include "../main_common.h"

void main_send_g_Type2(void);

void main_loop(void) {
    assert(IS_PROBE == 0);

    main_send_g_Type2();
    // Hauptschleife Empf�nger
    do {
        loopReceiverNormal();  // Funktion in threads.c
        loopReceiverOffline(); // Funktion in threads.c
    } while (1);
}
