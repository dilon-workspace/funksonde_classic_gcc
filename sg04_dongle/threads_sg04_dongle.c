/*
 * threads_sg04_dongle.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <msp430.h>
#include <stdio.h>
#include <string.h>
#include "../main_common.h"
#include "../messages_common.h"
#include "../info.h"

uint16_t g_wNukluart = 0;
uint16_t g_wNukl_last_SG04 = 0xFF;

static char g_pPCRec[MAXPCBUFFER]; // Pointer auf Empfangsmessage
static uint8_t g_ucPCRec = 0;      // Empfange Bytes
// static             // Zwischenstruktur f�r DataReceivedCheck
//#warning g_stInf brauchen wir das so?

#define ADC2UFD(NADC) (uint16_t)((NADC * 1500.0) / (4095.0 * 10.0 / 21.0))

bool PCReceivedCheck(void) {
    char c;
    int n, m, o;
    bool bRet = false;
    bool bRead = true;
    uint16_t w;

    while (bRead) {                                               // neue(s) Zeichen im Puffer
        bRead = receiveByteRS232(uart_com1, (unsigned char *)&c); // neues Zeichen holen

        if (!bRead)
            continue;

        if (g_ucPCRec >= MAXPCBUFFER) {
            for (n = 0; n < MAXPCBUFFER - 1; n++)
                g_pPCRec[n] = g_pPCRec[n + 1];
            g_ucPCRec--;
        }

        g_pPCRec[g_ucPCRec++] = c;

        if (g_ucPCRec == 0)
            continue;

        if (g_pPCRec[g_ucPCRec - 1] != '\r')
            continue;

        m = o = g_ucPCRec - 1;

        for (n = 1; n < o + 1; n++) {
            if (g_pPCRec[o - n] == '!') {
                m = o - n;
                break;
            }
        }

        if (m == o || o - m < 4) {
            g_ucPCRec = 0;
            Data_send_null_terminated_string(uart_com1, "!ERR\r\n");
            continue;
        }

        if (g_pPCRec[m + 1] == 'S' && g_pPCRec[m + 2] == 'S' && g_pPCRec[m + 3] == 'G') {
            if (g_ucModus != board_mode_reeiver_pc) {
                Data_send_null_terminated_string(uart_com1, "!SSGERR\r\n");
            }

            else {
                g_ucModus = board_mode_receiver_normal;
                Data_send_null_terminated_string(uart_com1, "!SSGOK\r\n");
            }

            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == 'S' && g_pPCRec[m + 2] == 'P' && g_pPCRec[m + 3] == 'C') {
            if (g_ucModus != board_mode_receiver_normal) {
                Data_send_null_terminated_string(uart_com1, "!SPCERR\r\n");
            } else {
                g_ucModus = board_mode_reeiver_pc;
                Data_send_null_terminated_string(uart_com1, "!SPCOK\r\n");
            }

            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == 'S' && g_pPCRec[m + 2] == 'N' && g_pPCRec[m + 3] == 'U') {
            Data_send_3_value(uart_com0, MSG_NUKLID, g_pPCRec[m + 4] - '0', 0, 0);
            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == 'I' && g_pPCRec[m + 2] == 'D' && g_pPCRec[m + 3] == 'T') {
            Data_send_null_terminated_string(uart_com1, "!IDT-SDM-XR\r\n");
            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == 'G' && g_pPCRec[m + 2] == 'N' && g_pPCRec[m + 3] == 'M') {
            Data_send_3_value(uart_com0, MSG_GETCMD, 3, 0, 0);
            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '#' && g_pPCRec[m + 2] == 'S' && g_pPCRec[m + 3] == 'N') {
            g_pPCRec[o] = 0;
            const size_t BUFFER_LENGTH = 100;
            char g_ucBuffer[BUFFER_LENGTH];
            n = snprintf(g_ucBuffer, BUFFER_LENGTH, "%s", &g_pPCRec[m + 4]);
            Data_send_string(uart_com0, MSG_SETNAME, (uint8_t *)g_ucBuffer, n);
            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '#' && g_pPCRec[m + 2] == 'G' && g_pPCRec[m + 3] == 'V') {
            Data_send_3_value(uart_com0, MSG_GETCMD, 1, 0, 0);
            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '#' && g_pPCRec[m + 2] == 'S' && g_pPCRec[m + 3] == 'V') {
            g_pPCRec[o] = 0;
            const size_t BUFFER_LENGTH = 100;
            char g_ucBuffer[BUFFER_LENGTH];
            n = snprintf(g_ucBuffer, BUFFER_LENGTH, "%s", &g_pPCRec[m + 4]);
            Data_send_string(uart_com0, MSG_SETVERSION, (uint8_t *)g_ucBuffer, n);
            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '#' && g_pPCRec[m + 2] == 'G' && g_pPCRec[m + 3] == 'S') {
            Data_send_3_value(uart_com0, MSG_GETCMD, 2, 0, 0);
            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '#' && g_pPCRec[m + 2] == 'S' && g_pPCRec[m + 3] == 'S') {
            g_pPCRec[o] = 0;
            const size_t BUFFER_LENGTH = 100;
            char g_ucBuffer[BUFFER_LENGTH];
            n = snprintf(g_ucBuffer, BUFFER_LENGTH, "%s", &g_pPCRec[m + 4]);
            Data_send_string(uart_com0, MSG_SETSERIAL, (uint8_t *)g_ucBuffer, n);
            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '*' && g_pPCRec[m + 2] == 'G' && g_pPCRec[m + 3] == 'V') {
            const size_t BUFFER_LENGTH = 100;
            char g_ucBuffer[BUFFER_LENGTH];
            snprintf(g_ucBuffer, BUFFER_LENGTH, "!*GV%s\r\n", g_stInfo.strVersion);
            Data_send_null_terminated_string(uart_com1, g_ucBuffer);
            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '*' && g_pPCRec[m + 2] == 'S' && g_pPCRec[m + 3] == 'V') {
            if (o - (m + 4) > 11) {
                Data_send_null_terminated_string(uart_com1, "!*SVERR\r\n");
                g_ucPCRec = 0;
                continue;
            } else {
                g_pPCRec[o] = 0;
                strINFO g_stInf = {0};
                memcpy(&g_stInf, &g_stInfo, sizeof(strINFO));
                snprintf(g_stInf.strVersion, sizeof(g_stInf.strVersion), "%s", &g_pPCRec[m + 4]);
                writeInfo(&g_stInf);
                Data_send_null_terminated_string(uart_com1, "!*SVOK\r\n");
                g_ucPCRec = 0;
                continue;
            }
        }

        if (g_pPCRec[m + 1] == '*' && g_pPCRec[m + 2] == 'G' && g_pPCRec[m + 3] == 'S') {
            const size_t BUFFER_LENGTH = 100;
            char g_ucBuffer[BUFFER_LENGTH];
            snprintf(g_ucBuffer, BUFFER_LENGTH, "!*GS%s\r\n", g_stInfo.strSerial);
            Data_send_null_terminated_string(uart_com1, g_ucBuffer);
            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '*' && g_pPCRec[m + 2] == 'S' && g_pPCRec[m + 3] == 'S') {
            if (o - (m + 4) > 11) {
                Data_send_null_terminated_string(uart_com1, "!*SSERR\r\n");
                g_ucPCRec = 0;
                continue;
            } else {
                g_pPCRec[o] = 0;
                strINFO g_stInf = {0};
                memcpy(&g_stInf, &g_stInfo, sizeof(strINFO));
                snprintf(g_stInf.strSerial, sizeof(g_stInf.strSerial), "%s", &g_pPCRec[m + 4]);
                writeInfo(&g_stInf);
                Data_send_null_terminated_string(uart_com1, "!*SSOK\r\n");
                g_ucPCRec = 0;
                continue;
            }
        }

        if (g_pPCRec[m + 1] == '#' && g_pPCRec[m + 2] == 'G' && g_pPCRec[m + 3] == 'U') {
            Data_send_3_value(uart_com0, MSG_GETCMD, 4, g_pPCRec[m + 4] - '0', 0);
            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '#' && g_pPCRec[m + 2] == 'S' && g_pPCRec[m + 3] == 'U' && g_pPCRec[m + 5] == ',') {
            uint16_t w1 = 0, w2 = 0;

            for (n = m + 6; n < o; n++) {
                if (g_pPCRec[n] == ',') {
                    g_pPCRec[n] = 0;
                    g_pPCRec[o] = 0;
                    w1 = atol(&g_pPCRec[m + 6]);
                    w2 = atol(&g_pPCRec[n + 1]);
                    break;
                }
            }

            if (w1 <= 2050 && w2 <= 2050) {
                Data_send_3_value(uart_com0, MSG_SETNUKLID, g_pPCRec[m + 4] - '0', w1, w2);
            } else {
                Data_send_null_terminated_string(uart_com1, "!#SUERR\r\n");
            }

            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '#' && g_pPCRec[m + 2] == 'O' && g_pPCRec[m + 3] == 'U') {
            uint16_t w1 = 0, w2 = 0;

            for (n = m + 4; n < o; n++) {
                if (g_pPCRec[n] == ',') {
                    g_pPCRec[n] = 0;
                    g_pPCRec[o] = 0;
                    w1 = atol(&g_pPCRec[m + 4]);
                    w2 = atol(&g_pPCRec[n + 1]);
                    break;
                }
            }

            if (w1 <= 2050 && w2 <= 2050) {
                Data_send_3_value(uart_com0, MSG_OUTNUKLID, w1, w2, 0);
            } else {
                Data_send_null_terminated_string(uart_com1, "!#OUERR\r\n");
            }

            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '#' && g_pPCRec[m + 2] == 'S' && g_pPCRec[m + 3] == 'T') {
            char g_ucBuffer[100]; // Zwischenbuffer f�r DataReceivedCheck
            g_ucBuffer[0] = '!';
            g_ucBuffer[1] = '#';
            g_ucBuffer[2] = 'S';
            g_ucBuffer[3] = 'T';
            g_ucBuffer[4] = '0';
            g_ucBuffer[5] = ',';
            g_ucBuffer[6] = '0';
            g_ucBuffer[7] = ',';
            g_ucBuffer[8] = '0';
            g_ucBuffer[9] = '\r';
            g_ucBuffer[10] = '\n';
            g_ucBuffer[11] = 0;

            if ((g_ucTransFlags & 0x01) > 0)
                g_ucBuffer[4] = '1';

            if ((g_ucTransFlags & 0x02) > 0)
                g_ucBuffer[6] = '1';

            if ((g_ucTransFlags & 0x04) > 0)
                g_ucBuffer[8] = '1';

            Data_send_null_terminated_string(uart_com1, g_ucBuffer);

            g_ucPCRec = 0;
            continue;
        }

        if (g_pPCRec[m + 1] == '#' && g_pPCRec[m + 2] == 'S' && g_pPCRec[m + 3] == 'R') {
            char g_ucBuffer[100]; // Zwischenbuffer f�r DataReceivedCheck
            g_ucBuffer[0] = '!';
            g_ucBuffer[1] = '#';
            g_ucBuffer[2] = 'S';
            g_ucBuffer[3] = 'R';
            g_ucBuffer[4] = '0';
            g_ucBuffer[5] = ',';
            g_ucBuffer[6] = '0';
            g_ucBuffer[7] = ',';
            g_ucBuffer[8] = '0';
            g_ucBuffer[9] = ',';
            g_ucBuffer[10] = '0';
            g_ucBuffer[11] = '\r';
            g_ucBuffer[12] = '\n';
            g_ucBuffer[13] = 0;

            if ((g_wErrVoltage & 0x01) > 0)
                g_ucBuffer[4] = '1';

            if ((g_wErrVoltage & 0x02) > 0)
                g_ucBuffer[6] = '1';

            if ((g_wErrVoltage & 0x3C) > 0)
                g_ucBuffer[8] = '1';

            if ((g_ucRecState & 0x01) > 0)
                g_ucBuffer[10] = '1';

            Data_send_null_terminated_string(uart_com1, g_ucBuffer);

            g_ucPCRec = 0;
            continue;
        }

        // MT101018 Offizieller Statusbefehl
        if (g_pPCRec[m + 1] == 'S' && g_pPCRec[m + 2] == 'T' && g_pPCRec[m + 3] == 'A') {
            char g_ucBuffer[100]; // Zwischenbuffer f�r DataReceivedCheck
            g_ucBuffer[0] = '!';
            g_ucBuffer[1] = 'S';
            g_ucBuffer[2] = 'T';
            g_ucBuffer[3] = 'A';
            g_ucBuffer[4] = '0';
            g_ucBuffer[5] = ',';
            g_ucBuffer[6] = '0';
            g_ucBuffer[7] = '\r';
            g_ucBuffer[8] = '\n';
            g_ucBuffer[9] = 0;

            if ((g_ucTransFlags & 0x01) > 0)
                g_ucBuffer[4] = '1';

            if ((g_ucRecState & 0x01) > 0)
                g_ucBuffer[6] = '1';

            Data_send_null_terminated_string(uart_com1, g_ucBuffer);

            g_ucPCRec = 0;
            continue;
        }

        Data_send_null_terminated_string(uart_com1, "!ERR\r\n");
        g_ucPCRec = 0;
    }
    return bRet;
}

// Empf�nger - Normalbetrieb
int loopReceiverNormal(void) {
    uint16_t wSec = 0;
    uint16_t wADC = 0;
    uint16_t wBT = 0;
    uint16_t wMV;
    bool oneSecondTickPassed;
    char ucBuffer[100];
    uint16_t wTicks;
    bool waiting_for_adc_result = false;
    UDOUT_PORT_DIR |= UDOUT_PINS; // P2.0 & P2.4 out
    UDOUT_PORT_SEL |= UDOUT_PINS; // P2.0 Timer A1/0 & P2.4 Timer A2/0
    initTimerA1();
    initTimerA2();
    g_bBTRLast = false;
    g_ucModus = board_mode_receiver_normal;
    powerOn();
    do {
        // Serielle Schnittstellen auswerten
        PCReceivedCheck();
        DataReceivedCheck();
        oneSecondTickPassed = false;
        // Jede Sekunde ADC-Konvertierung starten
        if (wSec != g_wSecTimer) {
#if 1
            P1OUT ^= 0x01;

#endif
            wSec = g_wSecTimer;
            startADC();
            waiting_for_adc_result = true;
        }
        // Wenn Konvertierung fertig, Mittelwert bilden
        if (g_ucADCCalcStart == true) {
            if (waiting_for_adc_result) {
                calcADC();
                oneSecondTickPassed = true;
            }
            waiting_for_adc_result = false;
        }
        if ((isWorkingInASG04())) {

            if (oneSecondTickPassed == true) {
                // nur beim ersten Mal und bei einer �nderung �ber BT rausschicken
                g_wNukl = g_wNukluart;
                Data_send_3_value(uart_com0, MSG_NUKLID, g_wNukl, 1, 0);
                if (g_wNukl_last_SG04 != g_wNukluart) {
                    g_wNukl_last_SG04 = g_wNukl;
                    g_wMSNuklChangeTime = g_wMSTimer + 1000;
                    g_bNuklNoChangeFlag = 0;
                } else {
                    //############################################################
                    // 27.08.2010, RL
                    // Verz�gerung f�r Ausgabe UD-OUT nach Nuklidwechsel
                    //###########################################################
                    // wenn es keine �nderung gab 1000Timeticks lang g_bNuklNoChangeFlag mit 1 �berschreiben
                    if (g_wMSNuklChangeTime < g_wMSTimer) {
                        g_bNuklNoChangeFlag = 1;
                    }
                }
            }
        } else {
            // Wenn neue ADC-Werte vorliegen
            if (wADC != g_pwMSADC[5]) {
                wADC = g_pwMSADC[5];
                // Analogsignal UFD verarbeiten
                if (g_ucModus != board_mode_reeiver_pc) {
                    wMV = ADC2UFD(g_pwADC[5]);          // in mV umrechnen
                    Data_send_2_value(MSG_ADC, wMV, 0); // als Analogsignal senden
                    g_wNukl_last = g_wNukl;             // letzte NuklidwahlschalterStellung speichern
                    if ((wMV + UFD_TOLERANZ) >= g_stInfo.pwTableRec[0][0] && (wMV - UFD_TOLERANZ) <= g_stInfo.pwTableRec[0][1])
                        g_wNukl = 1;
                    if ((wMV + UFD_TOLERANZ) >= g_stInfo.pwTableRec[1][0] && (wMV - UFD_TOLERANZ) <= g_stInfo.pwTableRec[1][1])
                        g_wNukl = 2;
                    if ((wMV + UFD_TOLERANZ) >= g_stInfo.pwTableRec[2][0] && (wMV - UFD_TOLERANZ) <= g_stInfo.pwTableRec[2][1])
                        g_wNukl = 3;
                    if ((wMV + UFD_TOLERANZ) >= g_stInfo.pwTableRec[3][0] && (wMV - UFD_TOLERANZ) <= g_stInfo.pwTableRec[3][1])
                        g_wNukl = 4;
                    if ((wMV + UFD_TOLERANZ) >= g_stInfo.pwTableRec[4][0] && (wMV - UFD_TOLERANZ) <= g_stInfo.pwTableRec[4][1])
                        g_wNukl = 5;
                    if ((wMV + UFD_TOLERANZ) >= g_stInfo.pwTableRec[5][0] && (wMV - UFD_TOLERANZ) <= g_stInfo.pwTableRec[5][1])
                        g_wNukl = 6;
                    if ((wMV + UFD_TOLERANZ) >= g_stInfo.pwTableRec[6][0] && (wMV - UFD_TOLERANZ) <= g_stInfo.pwTableRec[6][1])
                        g_wNukl = 7;

                    // MT101018 Data_send_2_value(MSG_NUKLID, g_wNukl);			 //als Nuklidsignal senden
                    Data_send_3_value(uart_com0, MSG_NUKLID, g_wNukl, 1, 0);

                    //############################################################
                    // 27.08.2010, RL
                    // Verz�gerung f�r Ausgabe UD-OUT nach Nuklidwechsel
                    //###########################################################
                    if (g_wNukl_last != g_wNukl) {
                        g_wMSNuklChangeTime = g_wMSTimer + 1000;
                        g_bNuklNoChangeFlag = 0;
                    } else {
                        if (g_wMSNuklChangeTime < g_wMSTimer)
                            g_bNuklNoChangeFlag = 1;
                    }
                }

                // Spannungspr�fung vornehmen
                g_wErrVoltage &= ~0x7F;

                if (g_pwADC[1] < P5V2ADC(LOWP5V))
                    g_wErrVoltage |= 0x01;
                if (g_pwADC[4] < P3V32ADC(MINP3V3))
                    g_wErrVoltage |= 0x04;
                if (g_pwADC[1] < P5V2ADC(MINP5V))
                    g_wErrVoltage |= 0x08;
                if ((g_wErrVoltage & 0x36) > 0) {
                    switchLowBatLED();
                } else {
                    if ((g_wErrVoltage & 0x01) > 0) {
                        onLowBatLED();
                    } else {
                        offLowBatLED();
                    }
                }
            }
        }

#ifdef STATUS8LED
        P2OUT = (g_wErrVoltage << 1) + ((~g_ucRecState) & 0x01);
#endif

#ifndef STATUS8LED
        // 15.02.2011, RL:
        // Ausgabe Verbindungsstatus an P2.0
        if (g_ucRecState & 1)
            clearBluetoothConnect(); // Verbindung getrennt
        else
            setBluetoothConnect();
#endif

        // Verbindungspr�fung
        if (g_wBTSLast > CHECKLINEINTERVAL) {
            if (g_bBTRLast == false) {
                g_wBTRLast = CHECKLINETIMEOUT + 1; // g_wSecTimer-CHECKLINETIMEOUT-1;
                g_bBTRLast = true;
            }
            Data_send_2_value(MSG_CHKLN, 0, 0);
#if 0
            {
					P1OUT ^= 0x01;
            }
#endif
            g_wBTSLast = 0;
        }

        if (wBT != g_wSecTimer) {
            wBT = g_wSecTimer;

            if (g_bBTRLast == false || g_wBTRLast > CHECKLINETIMEOUT) {
                switchStateLED();
                // switchStateLED2();
                g_ucRecState |= 0x01;
            } else {
                onStateLED();
                // switchStateLED2();
                g_ucRecState &= ~0x01;
            }
        }

    } while (1);

    g_ucOnOff = 0; // Taster nicht komplett best�tigt

#ifdef STATUS8LED
    P2OUT = 0;
#endif
    return 0;
}

// Empf�nger - Empf�nger aus
int loopReceiverOffline(void) {
    g_ucModus = board_mode_receiver_offline; // Sondenmodus festlegen

    // Alles stilllegen
    stoppTimerA1();
    // stoppTimerA2();
    powerOff();
    offRS232();
    offADC();
    // offDAC();
    offXT2Clock();
    offMeasBat();
    // Auf sparenden Clock schalten
    LPMClock();
    // LPM4 aktivieren
    _BIS_SR(OSCOFF + CPUOFF + SCG0 + SCG1 + GIE);
    // Erstmal mit Standard-Clock arbeiten
    defaultClock();
    // Hardware wieder einschalten
    onMeasBat();
    // onDAC();
    onADC();
    onXT2Clock();
    onRS232();

    g_ucOnOff = 0;
    return 0;
}

bool checkPower(uint8_t ucVCheckmode) {
    int n, nCnt = ADCSIZE + 1;
    int c, cCnt = ADCSIZE + 1;
    if (ucVCheckmode == 1) {
        nCnt = 1;
        for (n = 0; n < nCnt; n++) {
            startADC();
            while (g_ucADCCalcStart == false) {
            };
            for (c = 0; c < cCnt; c++) {
                calcADC();
            }
        }
    }
    if (ucVCheckmode == 2) {
        for (n = 0; n < nCnt; n++) {
            startADC();
            while (g_ucADCCalcStart == false) {
            }
            calcADC();
        }
    }
    if (ucVCheckmode == 3) {
        nCnt = 1;
        for (n = 0; n < nCnt; n++) {
            startADC();
            while (g_ucADCCalcStart == false) {
            }
            calcADC();
        }
    }
    if (g_pwADC[1] >= P5V2ADC(MINP5V)) {
        return true;
    }
    return false;
}
