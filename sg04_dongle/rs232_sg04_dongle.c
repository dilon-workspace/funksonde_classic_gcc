/*
 * rs232_sg04_dongle.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */
#include "../main_common.h"
#include "../rs232.h"
#include <string.h>

void working_in_sg04(uint8_t val) {
	if (val < NUKLIDMAX) {
		g_wNukluart = val + 1;
	}
}

void initRS232(void) {

	// Bluetooth-Port konfigurieren
	// P4.0 RTS  Input
	// P4.1 CTS  Output
	// P2.5 /DCD Input

	P4DIR &= ~0x07; // P4.0/P4.1/P4.2 als Input
	P2DIR &= ~0x20; // P2.5 als Input
	P4DIR |= 0x06;  // P4.1/P4.2 als Output
	P4SEL &= ~0x07; // P4.0, P4.1, P4.2 als I/O best�tigt
	P2SEL &= ~0x20; // P2.5 als I/O best�tigt
	// P4SEL2 &= ~0x07; // -""-
	// P2SEL2 &= ~0x20;
	P4REN &= ~0x07; // Alle ohne PullUpDown
	P2REN &= ~0x20; //
	P4OUT |= 0x04;  // P4.1/P4.2 High  original
	// P4OUT |= 0x06;   // P4.1/P4.2 High  w�re so korrekt, war es aber nicht

	// UART0-Port konfigurieren
	P3DIR &= ~0x10; // P3.4 Input
	P3DIR |= 0x08;  // P3.3 Output
	P3SEL |= 0x18;  // P3.3/P3.4 UART-Function
	// P3SEL2 &= ~0x18; // - "" -
	P3REN &= ~0x18; // P3.3/P3.4 ohne PullUpDown

	// UART1-Port konfigurieren
	P4DIR &= ~0x20; // P4.5 Input
	P4DIR |= 0x10;  // P4.4 Output
	P4SEL |= 0x30;  // P4.4/P4.5 UART-Function

	// P3SEL2 &= ~0x30; // - "" -
	P4REN &= ~0x30; // P4.4/P4.5 ohne PullUpDown

	// UART0 konfigurieren
	UCA0CTL0 = 0x00; // Keine Parit�t,LSB zuerst, 8Data,1Stopp, UART-Mode, Asynchron
	UCA0CTL1 = UCSSEL_3 + UCSWRST; // SMCLK, Softwarereset
	UCA0BR0 = 0x40;            // Baudratenteiler 0x0040: 7.3728e6 / 64 = 115200
	UCA0BR1 = 0x00;                // -""-
	UCA0MCTL = 0x00;               // Keine Bautratenmodulatoon
	UCA0CTL1 &= ~UCSWRST;          // Reset freigeben
	UCA0IE |= UCRXIE;              // Empfang aktivieren

	// UART1 konfigurieren
	UCA1CTL0 = 0x00; // Keine Parit�t,LSB zuerst, 8Data,1Stopp, UART-Mode, Asynchron
	UCA1CTL1 = UCSSEL_3 + UCSWRST; // SMCLK, Softwarereset

	if (isWorkingInASG04()) {
		UCA1BR0 = 0x80; // Baudratenteiler 0x0180: 7.3728e6 / 384 = 19200
		UCA1BR1 = 0x01; // -""-
	} else {
		UCA1BR0 = 0x40; // Baudratenteiler 0x0040: 7.3728e6 / 64 = 115200
		UCA1BR1 = 0x00; // -""-
	}

	UCA1MCTL = 0x00;      // Keine Bautratenmodulatoon
	UCA1CTL1 &= ~UCSWRST; // Reset freigeben
	UCA1IE |= UCRXIE;     // Empfang aktivieren
}

bool is_rs232_correct_mode(void) {
	if (g_ucModus == board_mode_receiver_offline) {
		return false;
	} else {
		return true;
	}
}
