/*
 * timer_sg04_dongle.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include "../main_common.h"
#include "../fifo.h"
#include "../histerese.h"
#include <string.h>

fifoBufferTmIntvl tickFifoBuffer;
extern uint16_t g_wBTicksToBeGeneratedA1;
extern uint16_t g_wBTicksToBeGeneratedA2;

#define INVALID_TIMER_VAL 0
#define INVALID_TICK_WINDOWTIMER_VAL 0xFFFF
uint16_t g_wTickWindowTimer = INVALID_TICK_WINDOWTIMER_VAL;
t_tmrIntervalData lastRxedIntervalTicks;

int32_t g_wTimeWindowSyncOffsetCorrection = 0;

void tmrToggleOsziTasterPin(void) {
	static uint8_t OsziDebugPinState = 0;
	isOsziTasterPinOut(); // P1.5 out
	if (OsziDebugPinState & 1)
		onOsziTasterPin();
	else
		offOsziTasterPin();
	OsziDebugPinState++;
}

void initTimerA1(void) {
	// 01.11.10, RL
	// Timer A1 für Ausgabe Ticks und Zählen Nuklidimpulse Kanal1

	fifoInitTmIntvl(&tickFifoBuffer);

	// Timer A1 für Ausgabe Ticks    //Empfänger

	TA1R = 0;
	TA1CCR0 = 55;        // Reine Defaults, die überschrieben werden
	TA1CCR1 = 37;        // Reine Defaults, die überschrieben werden
	TA1CCTL0 = CCIE;     // Interrupt beim Rücksetzen auslösen
	TA1CCTL1 = OUTMOD_3; //
	TA1CTL = // TBCLGRP_0 +                 //Timergroup 1+2,                 3+4...
			  // CNTL_0 +   // 16Bit Counter , nicht mehr vorhanden
			TASSEL_2 +       // SMCLK
					ID_3 +           // Teiler 8
					MC_0 +           // Timer steht
					TACLR +          // TAR autom. Rücksetzen
					0;
}

void initTimerA2(void) {

	// Timer A2 für Ausgabe Ticks und Zählen Nuklidimpulse
	fifoInitTmIntvl(&tickFifoBuffer);

	// Timer A2 für Ausgabe Ticks    //Empfänger

	TA2R = 0;
	TA2CCR0 = 55;        // Reine Defaults, die überschrieben werden
	TA2CCR1 = 37;        // Reine Defaults, die überschrieben werden
	TA2CCTL0 = CCIE;     // Interrupt beim Rücksetzen auslösen
	TA2CCTL1 = OUTMOD_3; //
	TA2CTL = // TBCLGRP_0 +                 //Timergroup 1+2,                 3+4...
			  // CNTL_0 +   // 16Bit Counter , nicht mehr vorhanden
			TASSEL_2 +       // SMCLK
					ID_3 +           // Teiler 8
					MC_0 +           // Timer steht
					TACLR +          // TAR autom. Rücksetzen
					0;
}

void calcTimeWindowCorrection(void) { // Aufruf aus writeTimerTicksToFifo (Empfängerbereich)
	const short TimeWindowGrowShrinkStep = 5 * BTWINDOW / 100; // 5 Prozent Fenster wachsen / oder schrumpfen lassen;;
	signed short timeOffset;
	t_histResult hr;
	char ucBuffer[100];

#if 5 * BTWINDOW / 100 == 0
#error BTWINDOW Too Small!
#endif
	static t_histData histData;
	uint16_t TickWindowTimer = g_wTickWindowTimer;
	g_wTimeWindowSyncOffsetCorrection = 0;
	if (histData.initialized == 0)
		// signed short HH, signed short HL, signed short LH, signed short LL
		histInitHigherLower_s16(&histData, BTWINDOW / 4, BTWINDOW / 8,
				-BTWINDOW / 8, -BTWINDOW / 4);

	if (TickWindowTimer == INVALID_TICK_WINDOWTIMER_VAL)
		return;
	timeOffset = TickWindowTimer - BTWINDOW / 2;
	hr = histCheckThr_s16(&histData, timeOffset);
	if (hr == hrThrH) {
		g_wTimeWindowSyncOffsetCorrection = TimeWindowGrowShrinkStep;
	}
	if (hr == hrThrL) {
		g_wTimeWindowSyncOffsetCorrection = -TimeWindowGrowShrinkStep;
	}
	// snprintf(ucBuffer, "Sync[ms]: %d %d\r\n",timeOffset,g_wTimeWindowSyncOffsetCorrection);
	// DebugSendEx(1,ucBuffer);
}

void calcTimerIntervalA1(t_tmrIntervalData *intervalData) { // Aufruf aus writeTimerTicksToFifo (Empfängerbereich)
	unsigned long lCnt;
	unsigned long TICKPERIOD_CLOCK = 7372800
			/ (8 * 1000 / (BTWINDOW + g_wTimeWindowSyncOffsetCorrection - 10)); // 230400
	const unsigned long TIMERA1_SIZEUS = 10UL;                 // on pulse in us
	const unsigned long long TIMERA1_SIZEON = (TIMERA1_SIZEUS
			* FUNKSONDEN_CLOCK_Hz) / (8UL * 1000UL * 1000UL); // 92 for 100us; 9 for 10us
	// Impulsverhältniss berechnen

	if (intervalData->intervalTicksA1 > 0) {
		lCnt = TICKPERIOD_CLOCK / intervalData->intervalTicksA1; // 230400 = 7.3728Mhz / 8(teiler) / 4(250ms)
		if (lCnt > 0xFFFF) {
			intervalData->intervallTotalA1 = 0xFFFF;
		} else {
			intervalData->intervallTotalA1 = lCnt;
		}
		if (intervalData->intervallTotalA1 < TIMERA1_SIZEON * 2) {
			intervalData->intervallTotalA1 = TIMERA1_SIZEON * 2;
		}
		intervalData->intervallOnA1 = intervalData->intervallTotalA1
				- TIMERA1_SIZEON;
	} else { // in case we get a 0 count value we just have to switch off the counter
		intervalData->intervallTotalA1 = INVALID_TIMER_VAL;
		intervalData->intervallOnA1 = INVALID_TIMER_VAL;
	}
}

void calcTimerIntervalA2(t_tmrIntervalData *intervalData) { // Aufruf aus writeTimerTicksToFifo (Empfängerbereich)
	unsigned long lCnt;
	unsigned long TICKPERIOD_CLOCK = 7372800
			/ (8 * 1000 / (BTWINDOW + g_wTimeWindowSyncOffsetCorrection - 10)); // 230400
	const unsigned long TIMERA2_SIZEUS = 10UL;                 // on pulse in us
	const unsigned long long TIMERA2_SIZEON = (TIMERA2_SIZEUS
			* FUNKSONDEN_CLOCK_Hz) / (8UL * 1000UL * 1000UL); // 92 for 100us; 9 for 10us

	// Impulsverhältniss berechnen
	if (intervalData->intervalTicksA2 > 0) {
		lCnt = TICKPERIOD_CLOCK / intervalData->intervalTicksA2; // 230400 = 7.3728Mhz / 8(teiler) / 4(250ms)
		if (lCnt > 0xFFFF) {
			intervalData->intervallTotalA2 = 0xFFFF;
		} else {
			intervalData->intervallTotalA2 = lCnt;
		}
		if (intervalData->intervallTotalA2 < TIMERA2_SIZEON * 2) {
			intervalData->intervallTotalA2 = TIMERA2_SIZEON * 2;
		}
		intervalData->intervallOnA2 = intervalData->intervallTotalA2
				- TIMERA2_SIZEON;
	} else { // in case we get a 0 count value we just have to switch off the counter
		intervalData->intervallTotalA2 = INVALID_TIMER_VAL;
		intervalData->intervallOnA2 = INVALID_TIMER_VAL;
	}
}

uint8_t loadTicksFromFifoToTimerA1(void) { // Aufruf aus Timerinterrupt
	t_tmrIntervalData intervallData;
	if (fifoReadTmIntvl(&tickFifoBuffer, &intervallData) == FIFO_SUCCESS) {
		g_wBTicksToBeGeneratedA1 = intervallData.intervalTicksA1;
		if (intervallData.intervallTotalA1 != INVALID_TIMER_VAL) {
			// Impulsverhältnisse setzen
			TA1CCR0 = intervallData.intervallTotalA1;
			TA1CCR1 = intervallData.intervallOnA1;
			startTimerA1();
		}
		return 1;
	} else {
		return 0;
	}
}

uint8_t loadTicksFromFifoToTimerA2(void) { // Aufruf aus Timerinterrupt
	t_tmrIntervalData intervallData;
	if (fifoReadTmIntvl(&tickFifoBuffer, &intervallData) == FIFO_SUCCESS) {
		g_wBTicksToBeGeneratedA2 = intervallData.intervalTicksA2;
		if (intervallData.intervallTotalA2 != INVALID_TIMER_VAL) {
			// Impulsverhältnisse setzen
			TA2CCR0 = intervallData.intervallTotalA2;
			TA2CCR1 = intervallData.intervallOnA2;
			startTimerA2();
		}
		return 1;
	} else {
		return 0;
	}
}

void writeTimerTicksToFifoA1(uint16_t wTicks) { // Aufruf von main.c Empfängerschleife bzw message.c (Empfängermessages)
	t_tmrIntervalData intervallData;
	// wTicks = 1000;
	intervallData.intervalTicksA1 = wTicks;
	calcTimerIntervalA1(&intervallData);
	calcTimeWindowCorrection();
	fifoWriteTmIntvl(&tickFifoBuffer, &intervallData);
	memcpy(&lastRxedIntervalTicks, &intervallData, sizeof(t_tmrIntervalData));
	tmrToggleOsziTasterPin();
	if (tickFifoBuffer.count == 1) {
		// first package? than let us misalign the time windows in such way that there is a higher timing tolerance for the transmission
		if (g_wTickWindowTimer == INVALID_TICK_WINDOWTIMER_VAL) {
			g_wTickWindowTimer = BTWINDOW / 2;
			// g_wTimeWindowSyncOffset  = BTWINDOW/2;
		} else {
			// g_wTimeWindowSyncOffset = g_wTickWindowTimer;
		}
	}
}

void writeTimerTicksToFifoA2(uint16_t wTicks1) { // Aufruf von main.c Empfängerschleife bzw message.c (Empfängermessages)
	t_tmrIntervalData intervallData;
	// wTicks = 1000;
	intervallData.intervalTicksA2 = wTicks1;
	calcTimerIntervalA2(&intervallData);
	calcTimeWindowCorrection();
	fifoWriteTmIntvl(&tickFifoBuffer, &intervallData);
	memcpy(&lastRxedIntervalTicks, &intervallData, sizeof(t_tmrIntervalData));
	tmrToggleOsziTasterPin();
	if (tickFifoBuffer.count == 1) {
		// first package? than let us misalign the time windows in such way that there is a higher timing tolerance for the transmission
		if (g_wTickWindowTimer == INVALID_TICK_WINDOWTIMER_VAL) {
			g_wTickWindowTimer = BTWINDOW / 2;
			// g_wTimeWindowSyncOffset  = BTWINDOW/2;
		} else {
			// g_wTimeWindowSyncOffset = g_wTickWindowTimer;
		}
	}
}

void post_isr_timer_a0(void) {
	if (g_wTickWindowTimer != INVALID_TICK_WINDOWTIMER_VAL)
		g_wTickWindowTimer++;
	if ((g_wTickWindowTimer >= BTWINDOW + g_wTimeWindowSyncOffsetCorrection)
			&& (g_wTickWindowTimer != INVALID_TICK_WINDOWTIMER_VAL)) {

		if (loadTicksFromFifoToTimerA1()) {
			// tmrToggleOsziShtDwnPin();
			loadTicksFromFifoToTimerA2();
			g_wTickWindowTimer = 0;
		} else {
			if ((g_ucRecState & 1) == 0) {   // wenn Verbindung weiter besteht..
				fifoWriteTmIntvl(&tickFifoBuffer, &lastRxedIntervalTicks); // if no package received in time, we should take the last one
				loadTicksFromFifoToTimerA1();
				loadTicksFromFifoToTimerA2();
				g_wTickWindowTimer = 0;
			} else {
				g_wTickWindowTimer = INVALID_TICK_WINDOWTIMER_VAL; // Wenn die BT Verbindung getrennt wurde..
				fifoInitTmIntvl(&tickFifoBuffer);
				// snprintf(ucBuffer, "inv.tickwindow\r\n");
				// DebugSendEx(1,ucBuffer);
			}
		}
	}
}
