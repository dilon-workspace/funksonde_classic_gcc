//-----------------------------------------------------------------------
// main.c
//
// Hauptprogramm
//  - Hauptschleife
//
//-----------------------------------------------------------------------
#include "main_common.h"
#include "rs232.h"
#include <stdio.h>
#include <string.h>

const uint32_t FUNKSONDEN_CLOCK_Hz = 7372800UL;
uint16_t g_wMSStartUpTime = 0; // Speicher f�r Startzeit
bool g_bMSStartUpFlag = false; // Flag f�r StartUpDelay abgeschlossen

void main_init(void);
void main_send_version(void);
void main_loop(void);

// Noch nicht behandelte m�gl. Interrupts
//#pragma interrupt_handler Watchdog_Timer_IR : WDT_VECTOR

__attribute__((interrupt(WDT_VECTOR))) void Watchdog_Timer_IR(void) {
}

//#pragma interrupt_handler NMI_IR : NMI_VECTOR
__attribute__((interrupt(UNMI_VECTOR))) void NMI_IR(void) {
}

// uint16_t g_wOnOff=0;	 	 	           								//wie lange?
// uint16_t g_wOnOffDown=0;	 					 						//wann gedr�ckt?
//---------------------------------------------------------------------------
// Hauptprogramm git test

// static void __attribute__((naked, section(".crt_0042"), used)) disable_watchdog(void) {
//    WDTCTL = WDTPW | WDTHOLD;
//}

int main(void) {

	// WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer // See function above

	// Erstinitialisierung
	WatchdogOff();

	_DINT();

	initClock(); // Clocks initialisieren
	initIO();    // I/O initialisieren

	onMeasBat();   // UBAT-Schalter ein, Batteriespannungsmessung freischalten
	initRS232();   // RS232 initialisieren
	initTimerA0(); // TimerA initialisieren (Systemuhr)
	initP1INT();   // Interrupts Port1 initalisieren
	initADC();     // ADC initialisieren
	_EINT();
	onXT2Clock(); // Auf volle Geschwindigkeit schalten
	main_init();

	main_send_version();
	Delay_ms(20);
	//#################################################
	// Hauptprogramm
	// Intro
	//#################################################
	// 27.08.2010, RL
	// Verz�gerung f�r Start-Up-Sequenz im SG03-Modus
	// -> f�r Anlaufverhalten der Sondenelektronik
	//    erfolgt Auswertung der Versorgungsspannungen
	g_wMSStartUpTime = g_wMSTimer + 11000;
	g_bMSStartUpFlag = 0;
	//#################################################
	main_loop();
	return 0;
}
