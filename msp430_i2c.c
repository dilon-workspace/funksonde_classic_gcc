/*
 * msp430_i2c.c
 *
 *  Created on: 17.12.2019
 *      Author: ak
 */

#include "MSP430F5xx_6xx/usci_b_i2c.h"
#include "../main_common.h"

typedef struct {
	uint8_t data_index;
	uint16_t data_length;
	uint8_t *txdata;
} ic2_transmission_state;

static ic2_transmission_state i2c_transmission_state;

//#############################################################################
// I2C - Interrupt
//#############################################################################

//------------------------------------------------------------------------------
// The USCIAB0TX_ISR is structured such that it can be used to transmit any
// number of bytes by pre-loading TXByteCtr with the byte count. Also, TXData
// points to the next byte to transmit.
//------------------------------------------------------------------------------
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = USCI_B0_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_B0_VECTOR)))
#endif
void USCIB0_ISR(void) {
	switch (__even_in_range(UCB0IV, USCI_I2C_UCBIT9IFG)) {
	case USCI_NONE: // No interrupts break;
		break;
	case USCI_I2C_UCALIFG: // Arbitration lost
		break;
	case USCI_I2C_UCNACKIFG: // NAK received (master only)
		// resend start if NACK
		USCI_B_I2C_masterSendStart(USCI_B0_BASE);
		break;
	case USCI_I2C_UCTXIFG: // TXIFG0
		// Check TX byte counter
		if (i2c_transmission_state.data_length
				> i2c_transmission_state.data_index) {
			USCI_B_I2C_masterSendMultiByteNext(USCI_B0_BASE,
					i2c_transmission_state.txdata[i2c_transmission_state.data_index]);
			i2c_transmission_state.data_index++;
			// Decrement TX byte counter
		} else {
			USCI_B_I2C_masterSendMultiByteStop(USCI_B0_BASE);
			// Exit LPM0
			__bic_SR_register_on_exit(CPUOFF);
		}
		break;
	default:
		break;
	}
}

void msp430_i2c_init(void) {
	USCI_B_I2C_initMasterParam param = { 0 };
	param.selectClockSource = USCI_B_I2C_CLOCKSOURCE_SMCLK;
	param.i2cClk = FUNKSONDEN_CLOCK_Hz;
	param.dataRate = USCI_B_I2C_SET_DATA_RATE_400KBPS;
	//  param.byteCounterThreshold = 0;
	// param.autoSTOPGeneration = USCI_B_I2C_NO_AUTO_STOP;
	USCI_B_I2C_initMaster(USCI_B0_BASE, &param);
}

void msp430_i2c_send_buffer(const uint8_t i2c_address, uint16_t buffer_length,
		uint8_t *buffer) {
	// Specify slave address
	USCI_B_I2C_setSlaveAddress(USCI_B0_BASE, i2c_address);

	// Set Master in receive mode
	USCI_B_I2C_setMode(USCI_B0_BASE, USCI_B_I2C_TRANSMIT_MODE);

	// Enable I2C Module to start operations
	USCI_B_I2C_enable(USCI_B0_BASE);
	USCI_B_I2C_clearInterrupt(USCI_B0_BASE,
	USCI_B_I2C_TRANSMIT_INTERRUPT + USCI_B_I2C_NAK_INTERRUPT);

	// Enable master Receive interrupt
	USCI_B_I2C_enableInterrupt(USCI_B0_BASE,
	USCI_B_I2C_TRANSMIT_INTERRUPT + USCI_B_I2C_NAK_INTERRUPT);

	i2c_transmission_state.data_index = 0;
	i2c_transmission_state.data_length = buffer_length;
	i2c_transmission_state.txdata = buffer;

	while (USCI_B_I2C_SENDING_STOP == USCI_B_I2C_masterIsStopSent(USCI_B0_BASE)) {
	}
	i2c_transmission_state.data_index++;
	USCI_B_I2C_masterSendMultiByteStart(USCI_B0_BASE,
			i2c_transmission_state.txdata[0]);

	__bis_SR_register(CPUOFF + GIE); // Enter LPM0 w/ interrupts
									 // Remain in LPM0 until all data
									 // is TX'd
									 // Increment data byte

	while (USCI_B_I2C_SENDING_STOP == USCI_B_I2C_masterIsStopSent(USCI_B0_BASE)) {
	}
}
