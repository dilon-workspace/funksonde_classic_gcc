//-----------------------------------------------------------------------
// p1int.c
//
// Interrupts des Ports 1
//  - Erkennung des Taster (gedr�ckt, nicht gedr�ckt)
//  - Erkennung und Z�hlung der Impulse der Sonde (UDIN)
//
//-----------------------------------------------------------------------
#include "main_common.h"

// Tempor�re Vars f�r Interrupt-Funktionen //
uint16_t g_wflg; // von P1INT_IR

// Ein-/Ausschalter
uint8_t g_ucOnOffDown = 0;              // Ein/Ausschalter gedr�ckt?
uint16_t g_wOnOffDown = 0;              // wann gedr�ckt?
uint8_t g_ucOnOff = 0;                  // Ein/Ausschalter komplett bet�tigt?
uint16_t g_wOnOff = 0;                  // wie lange?
uint8_t g_ucOnOfffort = 0;              // Ein/Ausschalter fortlaufend komplett bet�tigt?
uint8_t volatile g_ucOnOffDownfort = 0; // Ein/Ausschalter gedr�ckt?
uint16_t volatile g_wOnOfffort = 0;     // wann gedr�ckt?
uint16_t volatile g_wOnOffDownfort = 0; // wann gedr�ckt?

// TTL-Erfassung
uint16_t g_wMSStartMeasure = 0; // Startzeit in ms
uint16_t g_wOldMeasureA1 = 0;   // Anzahl der gem. Ticks, letzte Periode
uint16_t g_wOldMeasureA2 = 0;   // Anzahl der gem. Ticks, letzte Periode
uint8_t g_ucMeasure = 0;        // Daten letzter Periode bereit?

uint8_t g_ucTMeasure = 0; // Wird die Periode gerade gewechselt?

// BT-Verbindung
uint16_t volatile g_wBTSLast = 0; // Letzte gesendete Sekunde
uint16_t volatile g_wBTRLast = 0; // Letzte empfangende Sekunde
bool g_bBTRLast = false;          // empf. Sekunde g�ltig?

void initP1INT(void) {
    // 01.11.10, RL: Anpassung nicht genutzter P1.6
    // P1.4-P1.5 als Interrupts festlegen

    P1DIR &= ~0x30; // P1.4/P1.5 als Input
    P1SEL &= ~0x30; // P1.4/P1.5 Funktion, Rest als Standard-I/O
    // P1SEL2 &= ~0x30; // -""-
    P1REN &= ~0x30; // P1.4 mit PullUpDown
    P1REN |= 0x10;  // P1.4 mit PullUp
    P1OUT |= 0x10;  // P1.4 ohne PullUp
    P1IES &= ~0x30; // Low to High Flanke au�er P1.5
    P1IES |= 0x20;  // -""-
    P1IFG &= ~0x30; // Interrupts l�schen
    P1IE |= 0x30;   // P1.4/P1.5 Interrupt zulassen

    g_wMSStartMeasure = g_wMSTimer; // Startzeit
}

static void func_P1INTDEFAULT(void) {
}

static void func_P1INT4(void) // Tastendruck low to high
{
    if (g_ucOnOffDown == 0) // Verlassen wenn kompletter Tastendruck bereits registriert
        return;

    g_ucOnOffDown = 0;                    // Initialisierung des Tastendruck
    g_ucOnOffDownfort = 0;                // Initialisierung des Tastendruck
    g_ucOnOff = 1;                        // Taster komplett bet�tigt (loslassen)
    g_wOnOff = g_wMSTimer - g_wOnOffDown; // berechnen der gedr�ckten Abschaltzeit
}

static void func_P1INT5(void) // Tastendruck high to low (dr�cken)
{
    g_ucOnOff = 0;                 // Taster nicht komplett bet�tigt
    g_wOnOff = 0;                  // Tasterzeit wird initialisiert
    g_ucOnOffDown = 1;             // Taster bet�tigt
    g_ucOnOffDownfort = 1;         // Taster bet�tigt
    g_wOnOffDown = g_wMSTimer;     // Tastentimer initialisieren mit Systemzeit
    g_wOnOffDownfort = g_wMSTimer; // Tastentimer initialisieren mit Systemzeit
}

//#pragma interrupt_handler P1INT_IR : PORT1_VECTOR
__attribute__((interrupt(PORT1_VECTOR))) void P1INT_IR(void) {

    _BIC_SR_IRQ(OSCOFF + CPUOFF + SCG0 + SCG1); // LPM4, CPU and all clocks disabled, masked interrupts disabled

    g_wflg = P1IFG;
    P1IFG = 0;

    if (g_wflg & 0x10)
        func_P1INT4();

    if (g_wflg & 0x20)
        func_P1INT5();
    // if (g_wflg&0x40) func_P1INT6();
}
