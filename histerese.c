/*
 * histererese.c
 *
 *  Created on: 16.05.2014
 *      Author: ak
 *	a generic histerese state machine
 */


#include "histerese.h"
 


void histInitHigherLower_s16(t_histData *data, signed short HH, signed short HL, signed short LH, signed short LL){
	data->state = hsNone;
	data->mode = hmUsingHigherLower;
	data->thrHH = HH;
	data->thrHL = HL;
	data->thrLH = LH;
	data->thrLL = LL;
	data->initialized = 0;	
	if ((HH > HL) && (HL > LH) && (LH > LL))
		data->initialized = 1;
}

void histInitHigher_s16(t_histData *data, signed short HH, signed short HL){
	data->state = hsNone;
	data->mode = hmUsingHigher;
	data->thrHH = HH;
	data->thrHL = HL;
	data->thrLH = 0;
	data->thrLL = 0;
	data->initialized = 0;	
	if (HH > HL)
		data->initialized = 1;	
}

void histInitLower_s16(t_histData *data, signed short  LH, signed short  LL){
	data->state = hsNone;
	data->mode = hmUsingLower;
	data->thrHH = 0;
	data->thrHL = 0;
	data->thrLH = LH;
	data->thrLL = LL;
	data->initialized = 0;	
	if (LH > LL)
		data->initialized = 1;		
}

t_histResult histCheckThr_s16(t_histData *data, signed short value){
	t_histResult result = hrThrNo; 
	if (!data->initialized)
		return hrThrErr;
	switch(data->state){
		case hsHHhit:
			if ((data->mode == hmUsingHigherLower) || (data->mode == hmUsingHigher)){
				result = hrThrH;
				if (value < data->thrHL){
					data->state = hsHLhit;
					result = hrThrNo;
				}
			}
			break;
		case hsHLhit:
			if ((data->mode == hmUsingHigherLower) || (data->mode == hmUsingHigher)){
				result = hrThrNo;
				if (value > data->thrHH){
					data->state = hsHHhit;
					result = hrThrH;
				}		
			}
			break;
		case hsLHhit:
			if ((data->mode == hmUsingHigherLower) || (data->mode == hmUsingLower)){
				result = hrThrNo;
				if (value < data->thrLL){
					data->state = hsLLhit;
					result = hrThrL;
				}		
			}			
			break;
		case hsLLhit:
			if ((data->mode == hmUsingHigherLower) || (data->mode == hmUsingLower)){
				result = hrThrL;
				if (value > data->thrLH){
					data->state = hsLHhit;
					result = hrThrNo;
				}		
			}		
			break;
		case hsNone:
			if ((data->mode == hmUsingHigherLower) || (data->mode == hmUsingHigher)){
				result = hrThrNo;
				if (value > data->thrHH){
					data->state = hsHHhit;
					result = hrThrH;
				}		
			}	
			if ((data->mode == hmUsingHigherLower) || (data->mode == hmUsingLower)){
				result = hrThrNo;
				if (value < data->thrLL){
					data->state = hsLLhit;
					result = hrThrL;
				}		
			}					
			break;	
	}
	return result;
}
