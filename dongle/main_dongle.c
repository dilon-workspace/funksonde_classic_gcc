/*
 * main_dongle.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include <stdio.h>
#include "../main_common.h"
void main_send_version(void) {
	const size_t BUFFER_LENGTH = 200;
	char buffer[BUFFER_LENGTH];
	snprintf(buffer, BUFFER_LENGTH, g_Ident, g_stInfo.strVersion,
			g_stInfo.strSerial);
	Data_send_null_terminated_string(uart_com1, buffer);
}

void main_send_g_Type2(void) {
	Data_send_null_terminated_string(uart_com1, g_Type2);
}

void main_init(void) {
}

void soft_uart_tx_send_dual_nuklid_ticks(uint16_t ticks) {
	(void) ticks;
}
