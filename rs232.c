//-----------------------------------------------------------------------
// rs232.c
//
// Serielle Ports
//  - Ansprechen von UART0 und 1
//  - Ringbuffermethode zum Reinschreiben bzw. Rauslesen
//
//-----------------------------------------------------------------------
#include "main_common.h"
#include "main_spezial.h"
#include "rs232.h"
#include <string.h>

//#############################################################################
// globale und statische Variablen
//#############################################################################

#define UARTBUFFERSIZE 384
volatile unsigned char pucRXBuffer[uart_MAX][UARTBUFFERSIZE];
volatile unsigned char pucTXBuffer[uart_MAX][UARTBUFFERSIZE];
volatile unsigned short pwRXStart[uart_MAX] = {0, 0};
volatile unsigned short pwRXStopp[uart_MAX] = {0, 0};
volatile unsigned short pwTXStart[uart_MAX] = {0, 0};
volatile unsigned short pwTXStopp[uart_MAX] = {0, 0};

void working_in_sg04(uint8_t val);
//#############################################################################
// USART - Interrupts
//#############################################################################

__attribute__((interrupt(USCI_A0_VECTOR))) void UARTA0(void) {

    switch (__even_in_range(UCA0IV, 18)) {
        case 0x00: // Vector 0: No interrupts
            break;
        case 0x02:
            // Vector 2: UCRXIFG
            pucRXBuffer[uart_com0][pwRXStopp[uart_com0]++] = UCA0RXBUF; // Byte empfangen
            if (pwRXStopp[uart_com0] > UARTBUFFERSIZE)                  // Zeiger�berlauf abfangen
                pwRXStopp[uart_com0] = 0;
            if (pwRXStart[uart_com0] == pwRXStopp[uart_com0]) {
                pwRXStart[uart_com0]++;
                if (pwRXStart[uart_com0] > UARTBUFFERSIZE) {
                    pwRXStart[uart_com0] = 0;
                }
            }

            break;
        case 0x04:
            // Vector 4: UCTXIFG
            if (!(UCA0STAT & (1 << UCBUSY))) {                              // Abfrage ob grad gesendet oder empfangen wird
                UCA0TXBUF = pucTXBuffer[uart_com0][pwTXStart[uart_com0]++]; // Byte versenden
                if (pwTXStart[uart_com0] > UARTBUFFERSIZE)                  // Zeiger�berlauf abfangen
                    pwTXStart[uart_com0] = 0;
                if (pwTXStart[uart_com0] == pwTXStopp[uart_com0]) { // Wenn Ringbuffer leer, das Senden stoppen
                    // IE2 &= ~UCA0TXIE;
                    UCA0IE &= ~UCTXIE;
                }
            }
            break;
        case 0x06:
            // Vector 6: UCSTTIFG
            break;
        case 0x08:
            // Vector 8: UCTXCPTIFG
            break;
        default:
            break;
    }
}

//#pragma interrupt_handler UART1_TX : USCIAB1TX_VECTOR
__attribute__((interrupt(USCI_A1_VECTOR))) void UART1_TX(void) {

    switch (__even_in_range(UCA1IV, 18)) {
        case 0x00: // Vector 0: No interrupts
            break;
        case 0x02:
            // Vector 2: UCRXIFG
            // Wenn das Modul im SG04 arbeitet, bekommt es die Nuklideeinstellung �ber die UART.
            // Daher werden nur diese Daten akzeptiert und verarbeit.
            // In Verbingung mit einem PC werden weitere Befehle akzeptiert und die Bytes werden
            // regul�r in den Ringpuffer geschoben, von wo aus sie nachher verarbeitet werden.

            if (isWorkingInASG04()) { // kein Dongle?
                working_in_sg04(UCA1RXBUF);
            } else {
                pucRXBuffer[uart_com1][pwRXStopp[uart_com1]++] = UCA1RXBUF; // Byte empfangen

                if (pwRXStopp[uart_com1] > UARTBUFFERSIZE) { // Zeiger�berlauf abfangen
                    pwRXStopp[uart_com1] = 0;
                }
                if (pwRXStart[uart_com1] == pwRXStopp[uart_com1]) {
                    pwRXStart[uart_com1]++;
                    if (pwRXStart[uart_com1] > UARTBUFFERSIZE) {
                        pwRXStart[uart_com1] = 0;
                    }
                }
            }

            break;
        case 0x04:
            // Vector 4: UCTXIFG
            if (!(UCA1STAT & (1 << UCBUSY))) {                              // Abfrage ob grad gesendet oder empfangen wird
                UCA1TXBUF = pucTXBuffer[uart_com1][pwTXStart[uart_com1]++]; // Byte versenden
                if (pwTXStart[uart_com1] > UARTBUFFERSIZE) {                // Zeiger�berlauf abfangen
                    pwTXStart[uart_com1] = 0;
                }
                if (pwTXStart[uart_com1] == pwTXStopp[uart_com1]) { // Wenn Ringbuffer leer, das Senden stoppen
                    UCA1IE &= ~UCTXIE;
                }
            }
            break;
        case 0x06:
            // Vector 6: UCSTTIFG
            break;
        case 0x08:
            // Vector 8: UCTXCPTIFG
            break;
        default:
            break;
    }
}

//#############################################################################
// globale Funktionen
//#############################################################################

void onRS232(void) {
}

void offRS232(void) {
}

bool is_rs232_correct_mode(void);

void sendRS232(uart_com_index_t ucPort, unsigned char *pBuffer, unsigned short wSize) {
    // Variablen
    unsigned short wLen = 0;

    // Pr�fungen vornehmen
    if ((is_rs232_correct_mode() == false) && ucPort == 0) {
        return;
    }

    if (wSize < 1)
        return;

    if (wSize > UARTBUFFERSIZE)
        return;

    if (ucPort >= uart_MAX)
        return;

    if (pBuffer == 0)
        return;

    // Daten in den Ringbuffer kopieren
    for (; wSize > 0; wSize--) {
        pucTXBuffer[ucPort][pwTXStopp[ucPort]++] = pBuffer[wLen++];

        if (pwTXStopp[ucPort] > UARTBUFFERSIZE)
            pwTXStopp[ucPort] = 0;
    }

    // Falls noch nicht geschehen, das Senden starten

    if ((ucPort == uart_com0) && ((UCA0IE & UCTXIE) == 0)) {
        UCA0IFG |= UCTXIFG; //  und anwerfen
        UCA0IE |= UCTXIE;   // Sende-Interrupt freigeben
    }

    if (ucPort == uart_com1 && (UCA1IE & UCTXIE) == 0) {
        UCA1IFG |= UCTXIFG; //  und anwerfen
        UCA1IE |= UCTXIE;   // Sende-Interrupt freigeben
    }
}

bool receiveByteRS232(uart_com_index_t ucPort, unsigned char *puc) {
    // Pr�fungen vornehmen

    if (ucPort > 1)
        return false;

    if (puc == 0)
        return false;

    if (pwRXStart[ucPort] == pwRXStopp[ucPort])
        return false;

    *puc = pucRXBuffer[ucPort][pwRXStart[ucPort]++];

    if (pwRXStart[ucPort] > UARTBUFFERSIZE)
        pwRXStart[ucPort] = 0;

    return true;
}
