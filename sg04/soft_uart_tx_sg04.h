/*
 * soft_uart.h
 *
 *  Created on: 30.01.2020
 *      Author: ak
 */

#ifndef SOFT_UART_TX_H_
#define SOFT_UART_TX_H_

#include <stdint.h>

void soft_uart_tx_init(void);
void soft_uart_tx_send_byte(uint8_t byte);
void soft_uart_tx_send_dual_nuklid_ticks(uint16_t ticks);

#endif /* SOFT_UART_TX_H_ */
