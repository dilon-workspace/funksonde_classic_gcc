/*
 * soft_uart_tx.c
 *
 *  Created on: 30.01.2020
 *      Author: ak
 */

#include <msp430.h>
#include "../main_common.h"
#include "soft_uart_tx_sg04.h"
#include "soft_uart_fifo.h"

// P4.0
//#define PIN_TX_MASK (0x01 << 4)
//#define PIN_OUT P4OUT
//#define PIN_DIR P4DIR
//#define onLowBatLED() P2OUT |= 0x80     // LowbatLED einschalten
//#define offLowBatLED() P2OUT &= ~0x80   // LowbatLED ausschalten
#define Pin_TX_ON() P4OUT |= 0x10   // TX Pin high
#define Pin_TX_OFF() P4OUT &= ~0x10   // TX Pin low

#define TIMER_CTL_START_WORD TBSSEL_2 | ID_0 | MC_1 | TBIE
#define TIMER_CTL_STOP_WORD TBSSEL_2 | ID_0 | MC_0

static volatile uint8_t bit_counter = 0;
static volatile soft_uart_fifo_context_t soft_uart_fifo_context;
static volatile bool is_transmitting = false;

static void stop_transmission(void) {
	is_transmitting = false;
	P4SEL |= 0x10; // Pin4 auf UART Funktion
	TB0CTL = TIMER_CTL_STOP_WORD; // fertig und interrupt aus
}

__attribute__((interrupt(TIMER0_B0_VECTOR))) void isr_timer_b0(void) {
	static uint8_t byte_to_be_transmitted = 0;
	switch (TB0IV) // Read interrupt vector for TB0
	{
	case 0: // No interrupt pending
		break;
	case 2: // TB0CCR1
		break;
	case 4: // TB0CCR2
		break;
	case 6: // TB0CCR3
		break;
	case 8: // TB0CCR4
		break;
	case 10: // TB0CCR5
		break;
	case 12: // TB0CCR6
		break;
	case 14: // Overflow - TB0IFG
		P4SEL &= ~0x10; // PIN 4 auf I/O-Funktion
		bit_counter++;
		if (bit_counter == 1) {
			soft_uart_fifo_result_t fifo_result;
			fifo_result = soft_uart_fifo_read(&soft_uart_fifo_context,
					&byte_to_be_transmitted);
			if (fifo_result == fifo_success) {
				//PIN_OUT &= ~PIN_TX_MASK; // start bit
				Pin_TX_OFF();
				//offLowBatLED();

			} else {                     // es gibt wohl keine neuen Daten
				stop_transmission();
			}
		} else if (bit_counter < 10) {

			if (byte_to_be_transmitted & 0x1) {

				//PIN_OUT |= PIN_TX_MASK;
				Pin_TX_ON();
				//onLowBatLED();
			} else {
				//P2OUT ^= 0x80; // Toggle P1.0 using exclusive-OR
				//PIN_OUT &= ~PIN_TX_MASK;
				Pin_TX_OFF();
				//offLowBatLED();
			}
			byte_to_be_transmitted >>= 1;
		} else if (bit_counter == 10) {
			//PIN_OUT |= PIN_TX_MASK; // stop bit
			Pin_TX_ON();
			//onLowBatLED();
			bit_counter = 0;
			P4SEL |= 0x10; // Pin4 auf UART Funktion

		} else {
			P4SEL |= 0x10; // Pin4 auf UART Funktion
		}
		break;
	}
}

void soft_uart_tx_init(void) {

	const uint32_t BAUD_RATE = 4800UL;
	//PIN_DIR |= PIN_TX_MASK;
	P4DIR |= 0x10;
	//PIN_OUT |= PIN_TX_MASK; // Ausgang auf 1, der Standard IDLE zustand bei UART
	Pin_TX_ON();
	//onLowBatLED();
	TB0CCR0 = FUNKSONDEN_CLOCK_Hz / BAUD_RATE;
	TB0CCTL0 = CCIE;
	TB0CTL = TIMER_CTL_STOP_WORD; // Up Mode
	soft_uart_fifo_init(&soft_uart_fifo_context);
	_EINT();
#if 0
    uint16_t i = 0;
    for (;;) {
        i++;
        soft_uart_tx_send_dual_nuklid_ticks(i);
        Delay_ms(10);
    }
#endif
}

void soft_uart_tx_send_byte(uint8_t byte) {
	if (soft_uart_fifo_write(&soft_uart_fifo_context, byte) == fifo_success) {
		if (is_transmitting) {
			// es ist schon alles getan mit dem Schreiben in die FIFO
		} else {
			// wenn es eine frische Übertragung ist, muss noch zusätzlich alles in Gang gesetzt werden

			bit_counter = 0;
			is_transmitting = true;
			TB0CTL = TIMER_CTL_START_WORD;
			TB0CTL |= TBCLR; // timer auf 0 setzen und los gehts..
		}
	}
}

void soft_uart_tx_send_dual_nuklid_ticks(uint16_t ticks) {
	uint8_t lo_byte = ticks & 0x7F;
	uint8_t hi_byte = (ticks >> 7) & 0x7F;

	hi_byte |= 0x80;
	soft_uart_tx_send_byte(hi_byte);
	soft_uart_tx_send_byte(lo_byte);
}
