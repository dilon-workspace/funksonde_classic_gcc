/*
 * fifo.h
 *
 *  Created on: 23.04.2013
 *      Author: ak
 */

#ifndef FIFO_H_
#define FIFO_H_

#include <stdint.h>

#define BUFFER_SIZE 8                 // muss 2^n betragen (2, 4, 8, 16, 32, 64 ...)
#define BUFFER_MASK (BUFFER_SIZE - 1) // Klammern auf keinen Fall vergessen

typedef struct {
    uint8_t data[BUFFER_SIZE];
    uint8_t read;  // zeigt auf das Feld mit dem �ltesten Inhalt
    uint8_t write; // zeigt immer auf leeres Feld
    uint8_t count;
} soft_uart_fifo_context_t;

typedef enum { fifo_success, fifo_fail } soft_uart_fifo_result_t;

void soft_uart_fifo_init(volatile soft_uart_fifo_context_t *buffer);
soft_uart_fifo_result_t soft_uart_fifo_write(volatile soft_uart_fifo_context_t *buffer, uint8_t data);
soft_uart_fifo_result_t soft_uart_fifo_read(volatile soft_uart_fifo_context_t *buffer, uint8_t *data);

#endif /* FIFO_H_ */
