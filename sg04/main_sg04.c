/*
 * main_sg04.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include <stdio.h>
#include "../main_common.h"
#include "soft_uart_tx_sg04.h"

void main_send_version(void) {
    const size_t BUFFER_LENGTH = 200;
    char buffer[BUFFER_LENGTH];
    snprintf(buffer, BUFFER_LENGTH, g_Ident, g_stInfo.strVersion, g_stInfo.strSerial);
    Data_send_null_terminated_string(uart_com0, buffer);
}

void main_send_g_Type2(void) {
    Data_send_null_terminated_string(uart_com0, g_Type2);
}

void main_init(void) {
    soft_uart_tx_init();
}
