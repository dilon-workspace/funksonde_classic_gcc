/*
 * fifo.c
 *
 *  Created on: 23.04.2013
 *      Author: ak
 */

#include "soft_uart_fifo.h"

#include <string.h>

void soft_uart_fifo_init(volatile soft_uart_fifo_context_t *buffer) {
	buffer->read = 0;
	buffer->write = 0;
	buffer->count = 0;
}

soft_uart_fifo_result_t soft_uart_fifo_write(
		volatile soft_uart_fifo_context_t *buffer, uint8_t data) {
	uint8_t next = ((buffer->write + 1) & BUFFER_MASK);
	if (buffer->read == next)
		return fifo_fail;

	buffer->data[buffer->write] = data;
	buffer->write = next;
	buffer->count++;
	return fifo_success;
}

soft_uart_fifo_result_t soft_uart_fifo_read(
		volatile soft_uart_fifo_context_t *buffer, uint8_t *data) {
	if (buffer->read == buffer->write)
		return fifo_fail;
	*data = buffer->data[buffer->read];
	buffer->read = (buffer->read + 1) & BUFFER_MASK;
	buffer->count--;
	return fifo_success;
}
