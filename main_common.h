//-----------------------------------------------------------------------
// main.h
//
// Globale Festlegungen
//
//-----------------------------------------------------------------------

#ifndef MAIN_H
#define MAIN_H

#define __MSP430_HAS_USCI_Bx__
#include <stdint.h>
#include <stdbool.h>
#include <msp430.h>
#include "board.h"
#include "rs232.h"
#include "main_spezial.h"

//#############################################################################
// Typen test
//#############################################################################
//#define	uint32_t		unsigned long
//#define	uint16_t		unsigned short
//#define	uint8_t		unsigned char
//#define	bool		unsigned char
//#define	uint32_t		unsigned long
//#define	uint16_t		unsigned short
//#define	uint8_t		unsigned char
//#define	bool		unsigned char

#define true 1
#define false 0

//#define	SUCCESS		1
//#define	FAIL		0

typedef struct {
	uint16_t intervallTotalA1;
	uint16_t intervallTotalA2;
	uint16_t intervallOnA1;
	uint16_t intervallOnA2;
	uint16_t intervalTicksA1;
	uint16_t intervalTicksA2;
} t_tmrIntervalData;

extern const uint32_t FUNKSONDEN_CLOCK_Hz;
//#############################################################################
// Konstanten
//#############################################################################

// main.c
//#define DEBUG_
//#define STATUS8LED

// adc.c
#define ADCSIZE 3 // ADC Buffersize

// RL110823
#define UTHR_RESET 0x09C4 // Grundwert f�r UTHR, so das Schwelle maximal

#define NUKLIDMAX 13 // maximale Nuklidzahl
// timer.c
//#define TICKSBUFFERSIZE   2
// Timer B
/*
 #define TIMERB_CALCCR 379774.31  //Berechnungskonst. bei tperiode[�s]/freq[MHz]
 //tperiode= 350000.0
 //freq=     0.9216
 #define TIMERB_CALCCR 434027.78  //Berechnungskonst. bei tperiode[�s]/freq[MHz]
 //tperiode= 400000.0
 //freq=     0.9216
 #define TIMERB_CALCCR 221184.0  //Berechnungskonst. bei tperiode[�s]*freq[MHz]
 //tperiode= 240000.0
 //freq=     0.9216
 #define TIMERB_CALCCR 230400.0  //Berechnungskonst. bei tperiode[�s]*freq[MHz]
 //tperiode= 250000.0
 //freq=     0.9216
 #define TIMERB_CALCCR 211968.0  //Berechnungskonst. bei tperiode[�s]*freq[MHz]
 //tperiode= 230000.0
 //freq=     0.9216
 #define TIMERB_CALCCR 184320.0  //Berechnungskonst. bei tperiode[�s]*freq[MHz]
 //tperiode= 200000.0
 //freq=     0.9216
 #define TIMERB_CALCCR 228556.0  //Berechnungskonst. bei tperiode[�s]*freq[MHz]
 //tperiode= 248000.0
 //freq=     0.9216
 */
//#define TIMERB_CALCCR 225792.0  //Berechnungskonst. bei tperiode[�s]*freq[MHz]
// tperiode= 245000.0
// freq=     0.9216
//#define TIMERB_MINPERCNT 20
////Min. Periode, in Z�hlerwerten #define TIMERB_MAXPERCNT 65535
////Max. Periode, in Z�hlerwerten #define TIMERB_SIZEON 	 10
////L�nge Ein-Impuls, in Z�hlerwerten
// Nachricht verschickt wird (100ms im System SG04, 250ms im System Dongle
#define BTWINDOW 100 // Zeitfenster in dem die counts aufintegriert werden (SG04)
//#define BTWINDOW 250 // Zeitfenster in dem die counts aufintegriert werden (PC)
// message.c

#define MAXPCBUFFER 20 // Max. Gr��e des PC-Empfangsbuffers

// io.c
#define POWER_ON_DELAY 500 // Einschaltverz�gerungzeit in ms

// threads.c
#define MINUBAT 2100 // Minimal-mV f�r 7MHz (Sender)
#define LOWUBAT 2700 // Ubat niedrig, in mV

#define TESTUBAT 3100 // Ubat f�r Test
// 17.12.2010, RL
// 05.01.2011, RL: OFFUBAT -> 2,6V
#define OFFUBAT 2600  // Ubat, ab der die Sonde abgeschaltet wird
#define MINP5V 3700   // Minimal-mV f�r 7MHz (Empf�nger)
#define LOWP5V 4600   // 5V niedrig, in mV
#define MINP3V3 3100  // Minimal-mV 3,3V
#define MINN5V 4700   // Minimal-mV -5V
#define MINN31V 30000 // Minimal-mV -40V

#define CHECKLINEINTERVAL 3 // Interval der Verbindungspr�fung in s
#define CHECKLINETIMEOUT 8  // Verbindungs Timeout in s

#define LOWCOUNTS 5 // min. Anzahl der Counts/s f�r Arbeitsmodus
//#define SWITCHSLEEP		  300
////5min/Anz. Sekunde, wenn in Sleepmodus
#define SWITCHSLEEP 2700 // 45min/Anz. Sekunde, wenn in Sleepmodus
//#define SWITCHSLEEP 30 // 45min/Anz. Sekunde, wenn in Sleepmodus
//#define SWITCHOFFLINE 3 // 3 Sekunde, wenn in Ausschaltmodus, praktisch kein Sleepmodus vorhanden

#define FASTA 3  // Powercheck 1Sample Mittelwertbildung
#define NORMAL 2 // Powercheck Normal  Mittelwert von ADCSIZE Sample
#define FASTB 1  // Powercheck Schnell Mittelwert von 10*1 Sample

#define UFD_TOLERANZ 0 // UFD Schwellspannungen Toleranzfenster

// Definition PORT Pulse Kanal 1 zu SG04  an 5528 angepasst
#define UDOUT_PORT_DIR P2DIR
#define UDOUT_PORT_SEL P2SEL
#define UDOUT_PINS 0x11 // Pins f�r Kanal 1 & 2

// bt_conf.c
#define BT_BAUD_9600 0
#define BT_BAUD_115200 1
#define BT_CONF_BTM 0
#define BT_CONF_PC 1

//#############################################################################
// Makros
//#############################################################################
// adc.c
//#define ADC2UBAT(NADC) (uint16_t)((NADC * 1500.0) / (4095.0 * 10.0 / 26.0))
//#define ADC2P5V(NADC) (uint16_t)((NADC * 1500.0) / (4095.0 * 10.0 / 40.0))
//#define ADC2N5V(NADC) (uint16_t)((-(((NADC * 1500.0) / 4095.0) - 5000.0 * 47.0 / 80.0) / (1.0 - 47.0 / 80.0)))
//#define ADC2N40V(NADC) (uint16_t)((-(((NADC * 1500.0) / 4095.0) - 5000.0 * 300.0 / 330.0) / (1.0 - 300.0 / 330.0)))
//#define ADC2P3V3(NADC) (uint16_t)((NADC * 1500.0) / (4095.0 * 10.0 / 26.0))
//#define ADC2UFD(NADC) (uint16_t)((NADC * 1500.0) / (4095.0 * 10.0 / 21.0))

#define UBAT2ADC(voltage) (uint16_t)((voltage * 10.0 / 26.0 * 4095.0) / (1500.0))
#define P5V2ADC(voltage) (uint16_t)((voltage * 10.0 / 40.0 * 4095.0) / (1500.0))
#define N5V2ADC(voltage) (uint16_t)((-(4095.0 * (voltage - 47.0 / 80.0 * (voltage + 5000.0))) / 1500.0))
#define N31V2ADC(voltage) (uint16_t)((-(4095.0 * (voltage - 1000.0 / 120.0 * (voltage + 5000.0))) / 1500.0))
#define P3V32ADC(voltage) (uint16_t)((voltage * 10.0 / 26.0 * 4095.0) / (1500.0))
//#define UFD2ADC(voltage) (uint16_t)((voltage * 10.0 / 21.0 * 4095.0) / (1500.0))

// dac.c   ist den neuen Gegebenheiten noch anzupassen
#define ADC2MV(NADC) (uint16_t)((NADC * 1500.0 * (26.0) / 4095.0 * 10.0))

//#define onTestSignal()      P2OUT |= 0x02 //Testpuls einschalten #define
// offTestSignal()     P2OUT &= ~0x02      				  //Testpuls
// ausschalten

// timer.c
// Zeitverz�gerungen
#define Delay_ms(time) _delay(9100L * time / 10) // Verz�gerung in ms bei 7.3728 MHz und 7 Zyklen

// threads.c

// Watchdog
#define WatchdogOff() WDTCTL = WDTPW + WDTHOLD
#ifdef NDEBUG                             // NO debugging
#define Watchdog() WDTCTL = WDT_ARST_1000 // Watchdog mode, clear counter, ACLK 1000 ms
#else
#define Watchdog() // im DEBUG-Mode kein Watchdog
#endif
#define Reset() WDTCTL = 0 // Watchdog-Zugriff ohne Pa�wort -> PUC

//#############################################################################
// globale Variablen
//#############################################################################

// adc.c
extern uint16_t g_pwADC[8];            // ADC-Samples
extern uint16_t g_pwMSADC[8];          // ADC-Sample-Zeiten (MS)
extern volatile bool g_ucADCCalcStart; // ADC-Berechnung starten?

// Infospeicher, ist zu �ndern
// 92 Bytes, max. 256 Bytes
typedef struct {
	char strVersion[12];        // 0  Versionsnummer (SDM-XR)
	char strSerial[12];         // 12 Seriennummer (SDM-XR)
	char strName[12];           // 24 CrystalSondenname
	uint16_t pwTableTra[12][4]; // 36 Nuklidtabelle Sender
	uint16_t pwTableRec[12][4]; // 64 Nuklidtabelle	Empf�nger
} strINFO;
extern strINFO g_stInfo; // Infosegment

extern uint8_t g_ucTransFlags; // Flags des Empf�ngers (aktualisiert mit jeder Message)
// p1int.c

// Allgemein
extern uint16_t g_wMSStartMeasure;         // Startzeit in ms
extern uint8_t g_ucOnOff;                  // Ein/Ausschalter komplett bet�tigt?
extern uint16_t g_wOnOff;                  // wie lange?
extern uint16_t g_wOnOffDown;              // wie lange?
extern uint8_t g_ucOnOfffort;  // Ein/Ausschalter fortlaufend komplett bet�tigt?
extern uint8_t g_ucOnOffDown;              // Ein/Ausschalter gedr�ckt?
extern volatile uint8_t g_ucOnOffDownfort; // Ein/Ausschalter gedr�ckt?
extern volatile uint16_t g_wOnOfffort;   // fortlaufend kontrollierte Tastenzeit
extern volatile uint16_t g_wOnOffDownfort; // fortlaufend kontrollierte Tastenzeit

extern uint8_t g_ucMeasure;      // Daten letzter Periode bereit?
extern uint16_t g_wOldMeasureA1; // Anzahl der gem. Ticks Z�hler A1, letzte Periode
extern uint16_t g_wOldMeasureA2; // Anzahl der gem. Ticks Z�hler A1, letzte Periode

// BT-Verbindung
extern volatile uint16_t g_wBTSLast; // Letzte gesendete Sekunde
extern volatile uint16_t g_wBTRLast; // Letzte empfangende Sekunde
extern bool g_bBTRLast;              // empf. Sekunde g�ltig?

// main.c
extern uint16_t g_wMSStartUpTime; // Speicher f�r Startzeit
extern bool g_bMSStartUpFlag;     // Flag f�r StartUpDelay abgeschlossen

// messages.c
extern char g_Ident[];      // String zum Starten
extern char g_Type1[];      //. Sendetyp
extern char g_Type2[];      //. Empfangstyp
extern char g_PowerOn[];    //. Arbeitsmodus
extern char g_PowerSleep[]; //. Standby
extern char g_PowerOff[];   //. Ausschaltzustand

// extern strMESSAGE g_stMsgRec; // Zu letzt empfangendes Paket (BT-Verbindung)

// timer.c
extern volatile uint16_t g_wMSTimer;  // Systemzeit in ms
extern volatile uint16_t g_wSecTimer; // Systemzeit in s

// evt. doubeln
extern uint8_t g_ucTicksStart; // Tick-Ringbuffer, Startzeiger
extern uint8_t g_ucTicksStopp; // Tick-Ringbuffer, Stoppzeiger

// rs232.c

// threads.c
extern uint8_t g_ucModus;      // Arbeitsmodus
extern uint16_t g_wErrVoltage; // Fehlerstatus Spannungen
// Bit 0 = Low Bat
// Bit 1 = Min Bat
// Bit 2 = Min 3.3V
// Bit 3 = Min 5V
// Bit 4 = Min -5V
// Bit 5 = Min -40V
// Bit 6 = Test Bat
extern volatile uint8_t g_ucRecState; // Empf�ngerstatus
// Bit 0: 1=Verbindung nicht aufgebaut
extern uint16_t g_wNukl;             // Stellung Nuklidwahlschalter
extern uint16_t g_wNukl_last;        // letzte Stellung Nuklidwahlschalter
extern uint16_t g_wNukluart;         // Nuklid aus Uart1
extern uint8_t g_bNuklNoChangeFlag;  // Flag f�r "KEIN-NUKLID-WECHSEL"
extern uint16_t g_wMSNuklChangeTime; // Zeit des Nuklidwechsels

//#############################################################################
// Funktionsprototypen
//#############################################################################

// adc.c
void initADC(void);  // ADC initialisieren
void onADC(void);    //. einschalten
void offADC(void);   //. ausschalten
void startADC(void); // Konvertierungszyklus ansto�en
void calcADC(void);  // Mittelwertbildung durchf�hren

// dac.c    Umarbeitung auf I2C-Schnittstelle zum externen DAC notwendig
void initDAC(void); // DAC initialisieren
void onDAC(void);  //. einschalten    verweist urspr�nglich auch nur auf initDAC
void offDAC(void);  //. ausschalten
// DAC-Wert setzen
void setDAC(uint16_t w1, uint16_t w2, uint16_t w3, uint16_t w4);
void setDACPD(uint16_t w1, uint16_t w2, uint16_t w3, uint16_t w4);

// String auf bestimmten COM senden
void Data_send_null_terminated_string(uart_com_index_t ucPort, const char *data);
// Debug-String auf bestimmten COM senden
void DebugSendEx(uart_com_index_t ucPort, char *data);
// Datenpaket senden (COM0, Bluetooth)
void Data_send_2_value(uint8_t ucType, uint16_t wValue1, uint16_t wValue2);
// Datenpaket auf best. COM senden
void Data_send_3_value(uart_com_index_t ucPort, uint8_t ucType,
		uint16_t wValue1, uint16_t wValue2, uint16_t wValue3);
// String-Datenpaket auf best. COM senden
void Data_send_string(uart_com_index_t ucPort, uint8_t ucType, uint8_t *pB,
		uint8_t bLength);
bool DataReceivedCheck(void); // Seriellen Empfang pr�fen und verarbeiten (Bluetooth)
bool PCReceivedCheck(void);   // Seriellen Empfang pr�fen und verarbeiten
// (PC/USB)

// timer.c     einf�gen des 3. Timers, anpassen an Controller notwendig
void _delay( uint32_t);                         //�C Delay
void initTimerA0(void);                        // Timer A0 initialisieren
void initTimerA1(void);                        // Timer A1 initialisieren
void initTimerA2(void);                        // Timer A2 initialisieren
void startTimerA1(void);                       // Timer A1 starten
void startTimerA2(void);                       // Timer A2 starten
void writeTimerTicksToFifoA1(uint16_t wTicks); // Timer A1 mit Anz. Ticks starten
void writeTimerTicksToFifoA2(uint16_t wTicks); // Timer A2 mit Anz. Ticks starten
void stoppTimerA1(void); // Timer A1 stoppen   mu� getrennt bleiben da Aufruf aus Interrupt erfolgt
void stoppTimerA2(void);                       // Timer A2 stoppen
void checkTimerA12(void); // Timer A1 und A2 auslesen	/ in den Buffer schreiben	 //Ticks

// p1int.c
void initP1INT(void); // P6-Interrupts initialisieren
// void checkP1INT6(void);
// //P6.6-Interrupt pr�fen & ggf. verarbeiten (f�r Hauptfaden!) void
// startP1INT6(void);
// //P6.6-Interrupt freigeben void stoppP1INT6(void);
// //P6.6-Interrupt sperren

// io.c
void initIO(void); // I/Os initialisieren

#ifdef STATUS8LED
void testIO(void); // Testet IO
#endif

void powerOn(void);                             // Standbymodus aktivieren
void powerSleep(unsigned char ucIsTransmitter); // Sonde-aus-Modus aktivieren
void powerOff(void);
void exitLowBat(void); // Programmende, wenn Akku alle

// threads.c
int loopProbeNormal(void);             // Sendermode: Arbeitsmodus
int loopProbeSleep(void);              // Sendermode: Standby
int loopProbeOffline(void);            // Sendermode: Sonde aus
int loopReceiverNormal(void);          // Empf�ngermode: Arbeitsmodus
int loopReceiverOffline(void);         // Empf�ngermode: Aus
bool checkPower(uint8_t ucVCheckmode); // Minimalanforderung Akku pr�fen

// main.c

// clock.c
void initClock(void);    // Initialisiere Prozessorclock
void onXT2Clock(void);   // Schalte auf Ext. Quarz
void offXT2Clock(void);  // Schalte auf Int. Quarz
void LPMClock(void);     // Schalte auf LPM-Quarz
void defaultClock(void); // Schalte auf Standard Quarz (int)

// bt_conf.c
void bt_conf(void);                               // Routine f�r BTM-Config-Mode
void bt_conf_baud(unsigned char bt_baud);        // Baudrate f�r UCA0 umschalten
void bt_conf_copy(unsigned char ucSRC, unsigned char ucDEST); // Datenbytes weiterleiten
bool bt_conf_get_ok(void);           // Antwort von BT-Modul holen und auswerten

#endif
