//-----------------------------------------------------------------------
// info.c
//
// Infospeicher zum Ablegen von Sondeneinstellungen
//  - Firmware-Version
//  - Hardware-Seriennummer (SDM-XR)
//  - Herstellerkennzeichnung
//  - Nuklidtabelle
//
//-----------------------------------------------------------------------
#include "main_common.h"
//#include <msp430x26x.h>
#include <msp430.h>
#include <stddef.h>

// Infosegment f�r Funksonde mit SG04
strINFO g_stInfo = { "02.00-01/00\0", // Versionsnummer (SDM-XR)
		"0001\0",        // Seriennummer (SDM-XR)
		"CPxxxx\0",      // CrystalSondenname
						 // nachfolgende Werte sind Schwellenwerte in Funksonde in mV
						 // notwendiger Spannungswert = gew�nschte Energieschwelle in keV *3,4
						 // Nuklidtabelle Sender

		{ { 203, 68, 2500, 2500 }, // I-125		 0				 20keV - 60keV (mit CdTe-Kopf)
				{ 203, 68, 2500, 2500 }, // I-125		 1				 20keV - 60keV (mit CdTe-Kopf)
				{ 506, 304, 2500, 2500 }, // Co-57		 2				 90keV - 150keV
				{ 607, 337, 2500, 2500 }, // Tc-99m	 3				 100keV - 180keV
				{ 821, 573, 2500, 2500 }, // Lu-177	 4				 170keV - 245keV
				{ 936, 472, 2500, 2500 }, // In-111	 5				 140keV - 280keV
				{ 1353, 1067, 2500, 2500 }, // I-131		 6				 320keV - 410keV
				{ 1952, 1353, 2500, 2500 }, // PET		 7				 410keV - 610keV
				{ 2500, 68, 2500, 2500 }, // all		 8				 20keV - 60keV (mit CdTe-Kopf
				{ 607, 337, 203, 68 },      // Dual		 9	 
				{ 607, 337, 203, 68 }, // Dual		 10				wird nicht aufgerufen
				{ 2500, 68, 2500, 2500 } }, // all		 11				wird nicht aufgerufen

		// nachfolgende Werte sind Schwellenwerte f�r Fenstergrenzen von DAC�s im SG04 in mV
		// Nuklidtabelle	Empf�nger;				nicht mehr relevant
		{ { 50, 70, 2500, 2500 },      // I-129
				{ 180, 315, 2500, 2500 },    // Tl-201
				{ 360, 400, 2500, 2500 },    // Tc-99m
				{ 950, 1100, 2500, 2500 },   // I-131
				{ 1250, 1450, 2500, 2500 },  // PET
				{ 320, 350, 2500, 2500 },    // Co-57
				{ 450, 500, 2500, 2500 },    // In-111
				{ 240, 2500, 2500, 2500 },   // leer
				{ 240, 2500, 2500, 2500 },   // leer
				{ 240, 2500, 2500, 2500 },   // leer
				{ 240, 2500, 2500, 2500 },   // leer
				{ 240, 2500, 2500, 2500 } } }; // all		 10				 70

//#pragma end_abs_address

void writeInfo(strINFO *pInfo) {
#warning muss wieder aktiviert werden?
#if 0
    uint8_t *p1 = (uint8_t *)&g_stInfo;
    uint8_t *p2 = (uint8_t *)pInfo;
    uint8_t *p1 = (uint8_t *)&g_stInfo;
    FCTL2 = FWKEY + FSSEL0 + FN1; // MCLK/3 for Flash Timing Generator
    FCTL1 = FWKEY + ERASE;        // Set Erase bit
    FCTL3 = FWKEY + LOCKA;        // Clear LOCK & LOCKA bits
    p1[0] = 0;
    FCTL1 = FWKEY + WRT; // Set WRT bit for write
    uint8_t *p2 = (uint8_t *)pInfo;
    for (size_t n = 0; n < sizeof(strINFO); n++) {
        p1[n] = p2[n];
    }

    FCTL1 = FWKEY;                // Clear WRT bit
    FCTL3 = FWKEY + LOCKA + LOCK; // Set LOCK & LOCKA bit
    FCTL2 = FWKEY + FSSEL0 + FN1; // MCLK/3 for Flash Timing Generator
    Delay_ms(100);
#endif
}
