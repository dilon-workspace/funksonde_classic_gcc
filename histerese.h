/*
 * histerese.h
 *
 *  Created on: 16.05.2014
 *      Author: ak
 */

#ifndef HISTERESE_H_
#define HISTERESE_H_

typedef enum{hsNone=0,hsHHhit,hsHLhit,hsLHhit,hsLLhit} t_histState;
typedef enum{hrNone=0,hrThrNo,hrThrH,hrThrL,hrThrErr} t_histResult;
typedef enum{hmNone=0,hmUsingHigherLower,hmUsingHigher,hmUsingLower} t_histMode;

typedef struct
{
	signed short thrHH;
	signed short thrHL;
	signed short thrLH;
	signed short thrLL;
	t_histState state;
	t_histMode mode;
	int initialized;
} t_histData;

void histInitHigherLower_s16(t_histData *data, signed short HH, signed short HL, signed short LH, signed short LL);
void histInitHigher_s16(t_histData *data, signed short HH, signed short HL);
void histInitLower_s16(t_histData *data, signed short LH, signed short LL);

t_histResult histCheckThr_s16(t_histData *data, signed short value);

#endif /* HISTERESE_H_ */
