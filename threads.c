/*
 * threads.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */
#include <stdint.h>
#include "main_spezial.h"

uint16_t g_wErrVoltage = 0x00;        // Bit 7 = Testbatteriespannung; Bit 6 = HV; Bit 5 = -5V; Bit 4 = +5V; Bit 1 = Ubat;
uint16_t g_wNukl = 0;                 // Stellung Nuklidwahlschalter
uint8_t g_bNuklNoChangeFlag = 0;      // Anzeige, das Nuklid nicht gewechselt wurde
volatile uint8_t g_ucRecState = 0x01; // Empfängerstatus Bit 0: 1=Verbindung nicht aufgebaut
uint16_t g_wNukl_last = 0;            // letzte Stellung Nuklidwahlschalter
uint16_t g_wMSNuklChangeTime = 0;     // Zeit des Nuklidwechsels
board_mode_t g_ucModus = 0;
