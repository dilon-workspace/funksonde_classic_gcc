/*
 * fifo.c
 *
 *  Created on: 23.04.2013
 *      Author: ak
 */

#include "fifo.h"
#include <string.h>

#if FIFOUSE16
void fifoInit16(fifoBuffer16 *buffer){
	buffer->read = 0;
	buffer->write = 0;
}

uint8_t fifoWrite16(fifoBuffer16 *buffer, uint16_t word){
	uint8_t next = ((buffer->write + 1) & BUFFER_MASK);
	if (buffer->read == next)
		return FAIL;
	buffer->data[buffer->write] = word;
	//buffer.data[buffer.write & BUFFER_MASK] = byte; // absolut Sicher
	buffer->write = next;
	return SUCCESS;
}

uint8_t fifoRead16(fifoBuffer16 *buffer,uint16_t *word){
	if (buffer->read == buffer->write)
		return FAIL;
	*word = buffer->data[buffer->read];
	buffer->read = (buffer->read+1) & BUFFER_MASK;
	return SUCCESS;
}
#endif

#if FIFOUSE8
void fifoInit8(fifoBuffer8 *buffer){
	buffer->read = 0;
	buffer->write = 0;
}

uint8_t fifoWrite8(fifoBuffer8 *buffer, uint8_t byte){
	uint8_t next = ((buffer->write + 1) & BUFFER_MASK);
	if (buffer->read == next)
		return FAIL;
	buffer->data[buffer->write] = byte;
	// buffer.data[buffer.write & BUFFER_MASK] = byte; // absolut Sicher
	buffer->write = next;
	return SUCCESS;
}

uint8_t fifoRead8(fifoBuffer8 *buffer, uint8_t *pByte){
	if (buffer->read == buffer->write)
		return FAIL;
	*pByte = buffer->data[buffer->read];
	buffer->read = (buffer->read+1) & BUFFER_MASK;
	return SUCCESS;
}
#endif


#if FIFOUSETmIntvl
void fifoInitTmIntvl(volatile fifoBufferTmIntvl *buffer) {
	buffer->read = 0;
	buffer->write = 0;
	buffer->count = 0;
}


uint8_t fifoWriteTmIntvl(volatile fifoBufferTmIntvl *buffer, volatile t_tmrIntervalData *data){
	uint8_t next = ((buffer->write + 1) & BUFFER_MASK);
	if (buffer->read == next)
		return FIFO_FAIL;
	memcpy((t_tmrIntervalData *)&buffer->data[buffer->write],(t_tmrIntervalData *)data,sizeof(t_tmrIntervalData));
	// buffer.data[buffer.write & BUFFER_MASK] = byte; // absolut Sicher
	buffer->write = next;
	buffer->count++;
	return FIFO_SUCCESS;
}

uint8_t fifoReadTmIntvl(volatile fifoBufferTmIntvl *buffer, t_tmrIntervalData *data){
	if (buffer->read == buffer->write)
		return FIFO_FAIL;
	memcpy(data, (t_tmrIntervalData *)&buffer->data[buffer->read], sizeof(t_tmrIntervalData));
	buffer->read = (buffer->read+1) & BUFFER_MASK;
	buffer->count--;
	return FIFO_SUCCESS;
}

#endif

