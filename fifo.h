/*
 * fifo.h
 *
 *  Created on: 23.04.2013
 *      Author: ak
 */

#ifndef FIFO_H_
#define FIFO_H_

#include "main_common.h"

#define FIFO_FAIL 0
#define FIFO_SUCCESS 1

#define FIFOUSE8 0
#define FIFOUSE16 0
#define FIFOUSETmIntvl 1

#define BUFFER_SIZE 4                 // muss 2^n betragen (2, 4, 8, 16, 32, 64 ...)
#define BUFFER_MASK (BUFFER_SIZE - 1) // Klammern auf keinen Fall vergessen

#if FIFOUSE16
typedef struct {
    uint16_t data[BUFFER_SIZE];
    uint8_t read;  // zeigt auf das Feld mit dem �ltesten Inhalt
    uint8_t write; // zeigt immer auf leeres Feld
} fifoBuffer16;
#endif

#if FIFOUSE8
typedef struct {
    uint8_t data[BUFFER_SIZE];
    uint8_t read;  // zeigt auf das Feld mit dem �ltesten Inhalt
    uint8_t write; // zeigt immer auf leeres Feld
} fifoBuffer8;
#endif

#if FIFOUSETmIntvl
typedef struct {
    t_tmrIntervalData data[BUFFER_SIZE];
    uint8_t read;  // zeigt auf das Feld mit dem �ltesten Inhalt
    uint8_t write; // zeigt immer auf leeres Feld
    uint8_t count;
} fifoBufferTmIntvl;
#endif

#if FIFOUSE16
void fifoInit16(fifoBuffer16 *buffer);
uint8_t fifoWrite16(fifoBuffer16 *buffer, uint16_t word);
uint8_t fifoRead16(fifoBuffer16 *buffer, uint16_t *word);
#endif

#if FIFOUSE8
void fifoInit8(fifoBuffer8 *buffer);
uint8_t fifoWrite8(fifoBuffer8 *buffer, uint8_t byte);
uint8_t fifoRead8(fifoBuffer8 *buffer, uint8_t *pByte);
#endif

#if FIFOUSETmIntvl
void fifoInitTmIntvl(volatile fifoBufferTmIntvl *buffer);
uint8_t fifoWriteTmIntvl(volatile fifoBufferTmIntvl *buffer, volatile t_tmrIntervalData *data);
uint8_t fifoReadTmIntvl(volatile fifoBufferTmIntvl *buffer, t_tmrIntervalData *data);
#endif

#endif /* FIFO_H_ */
