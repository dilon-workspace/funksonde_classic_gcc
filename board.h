/*
 * board.h
 *
 *  Created on: 30.10.2019
 *      Author: ak
 */

#ifndef BOARD_H_
#define BOARD_H_

// io.c
//#define isReceiver() (P4IN & 0x80) // Abfrage Arbeitsmodus Transmitter/Receiver

#define onStateLED() P2OUT |= 0x40      // StatusLED einschalten
#define offStateLED() P2OUT &= ~0x40    // StatusLED ausschalten
#define switchStateLED() P2OUT ^= 0x40  // Zustand StatusLED wechseln
#define onLowBatLED() P2OUT |= 0x80     // LowbatLED einschalten
#define offLowBatLED() P2OUT &= ~0x80   // LowbatLED ausschalten
#define switchLowBatLED() P2OUT ^= 0x80 // Zustand LowbatLED wechseln

#define onTestSignal1() P2OUT |= 0x01   // Testpuls1 einschalten Counts f�r SG04
#define offTestSignal1() P2OUT &= ~0x01 // Testpuls1 ausschalten
#define onTestSignal2() P2OUT |= 0x10   // Testpuls2 einschalten
#define offTestSignal2() P2OUT &= ~0x10 // Testpuls2 ausschalten

#define onMeasBat() P1OUT |= 0x08   // UBAT-Pr�fung einschalten
#define offMeasBat() P1OUT &= ~0x08 // UBAT-Pr�fung abschalten

#define offShutdownProbe() P1OUT |= 0x80 // Shutdown Probe off
#define onShutdownProbe() P1OUT &= ~0x80 // Shutdown Probe on

#define offBluetooth() P4OUT &= ~0x08 // Bluetooth-modul off
#define onBluetooth() P4OUT |= 0x08   // Bluetooth-modul on

#define isWorkingInASG04()                                                                                                                           \
    (P1IN & 0x40)                              // Der Z�hlimpulseeingang. Wird im Receivermodus daf�r genutzt zu
                                               // erkennen, ob die Schwellen per 19.2k Baud UART
                                               //�bertragen werden(high) oder analog(Low).
#define setBluetoothConnect() P3OUT |= 0x04    // Bluetooth-modul on
#define clearBluetoothConnect() P3OUT &= ~0x04 // Bluetooth-modul off

#define onOsziTasterPin() P1OUT |= 0x20 // Tastereingang
#define offOsziTasterPin() P1OUT &= ~0x20
#define isOsziTasterPinOut() P1DIR |= 0x20

#define onOsziShtDwnPin() P1OUT |= 0x80;
#define offOsziShtDwnPin() P1OUT &= ~0x80;

#define bt_conf_btm_on() (P4OUT |= 0x08)
#define bt_conf_btm_off() (P4OUT &= ~0x08)

#endif /* BOARD_H_ */
