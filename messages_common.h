/*
 * messages_common.h
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#ifndef MESSAGES_COMMON_H_
#define MESSAGES_COMMON_H_

#include <stdint.h>

typedef struct {
    uint8_t ucStart;    // 0 Startbyte 0x02
    uint8_t ucType;     // 1 Messagetyp 0x00=Messwerte Ticks (Sonde->PC)
                        //       		0x01=Messwerte ADC (PC->Sonde)
                        //              ..
    uint8_t ucFlags;    // 2 Flags Bit0:LowBat
    uint8_t ucCount;    // 3 Paketnummer
    uint8_t ucData[12]; // 4 Daten
    uint8_t ucChks;     // 16 Checksumme (alles au�er Start/Stopp/Chks)
    uint8_t ucStopp;    // 17 Stoppbyte 0x03
} strMESSAGE;

extern const uint8_t MSG_TICKS;      // Messwert (Z�hlerstand)
extern const uint8_t MSG_ADC;        // Messwert (ADC/R�ckkanal)
extern const uint8_t MSG_NUKLID;     // Messwert als Nuklidwahl (ADC/R�ckkanal)
extern const uint8_t MSG_CHKLN;      // Check Line Anfrage
extern const uint8_t MSG_ACKLN;      // Check Line Antwort
extern const uint8_t MSG_CMDERR;     // MT101018 Command Error
extern const uint8_t MSG_CMDOK;      // MT101018 Command OK
extern const uint8_t MSG_PROBESTAT;  // Sondenzustand
extern const uint8_t MSG_GETCMD;     // Kommando zum Auslesen
extern const uint8_t MSG_GETVERSION; // Versionsnummer auslesen
extern const uint8_t MSG_SETVERSION; // Versionsnummer setzen
extern const uint8_t MSG_GETSERIAL;  // Seriennummer auslesen
extern const uint8_t MSG_SETSERIAL;  // Seriennummer setzen
extern const uint8_t MSG_GETNAME;    // Sondenname auslesen
extern const uint8_t MSG_SETNAME;    // Sondenname setzen
extern const uint8_t MSG_GETNUKLID;  // Nuklidpaar auslesen
extern const uint8_t MSG_SETNUKLID;  // Nuklidpaar setzen
extern const uint8_t MSG_OUTNUKLID;  // Nuklidpaar ausgeben

#endif /* MESSAGES_COMMON_H_ */
