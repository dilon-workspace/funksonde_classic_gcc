/*
 * rs232_sonde.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include "../main_common.h"
#include "../rs232.h"
#include <string.h>

void initRS232(void) {
    // Bluetooth-Port konfigurieren
    // P4.0 RTS  Input
    // P4.1 CTS  Output
    // P4.2 RST  Output
    // /DCD Input, vorher P5.5(SMCLK) jetzt erstmal P2.5
    P4DIR &= ~0x07; // P4.0/P4.1/P4.2 als Input
    P2DIR &= ~0x20; // P2.5 als Input
    P4DIR |= 0x06;  // P4.1/P4.2 als Output
    P4SEL &= ~0x07; // P4.0, P4.1, P4.2 als I/O best�tigt
    P2SEL &= ~0x20; // P2.5 als I/O best�tigt
    // P4SEL2 &= ~0x07; // -""-
    // P2SEL2 &= ~0x20;
    P4REN &= ~0x07; // Alle ohne PullUpDown
    P2REN &= ~0x20; //
    P4OUT |= 0x04;  // P4.1/P4.2 High  original
    // P4OUT |= 0x06;   // P4.1/P4.2 High  w�re so korrekt, war es aber nicht

    // UART0-Ports konfigurieren
    P3DIR &= ~0x10; // P3.4 Input
    P3DIR |= 0x08;  // P3.3 Output
    P3SEL |= 0x18;  // P3.3/P3.4 UART-Function
    // P3SEL2 &= ~0x18; // - "" -
    P3REN &= ~0x18; // P3.4/P3.5 ohne PullUpDown

    // UART0 konfigurieren
    UCA0CTL0 = 0x00;               // Keine Parit�t,LSB zuerst, 8Data,1Stopp, UART-Mode, Asynchron
    UCA0CTL1 = UCSSEL_3 + UCSWRST; // SMCLK, Softwarereset
    UCA0BR0 = 0x40;                // Baudratenteiler 0x0040: 7.3728e6 / 64 = 115200
    UCA0BR1 = 0x00;                // -""-
    UCA0MCTL = 0x00;               // Keine Bautratenmodulatoon
    UCA0CTL1 &= ~UCSWRST;          // Reset freigeben
    UCA0IE |= UCRXIE;              // Empfang aktivieren
}

void working_in_sg04(uint8_t val) {
}

bool is_rs232_correct_mode(void) {
    if ((g_ucModus == board_mode_probe_sleep) || (g_ucModus == board_mode_probe_offline)) {
        return false;
    } else {
        return true;
    }
}
