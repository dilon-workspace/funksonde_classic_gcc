/*
 * timer_sonde.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include "../main_common.h"

#define NOFIFO IS_PROBE

void post_isr_timer_a0(void) {
}

void initTimerA1(void) {
// 01.11.10, RL
// Timer A1 f�r Ausgabe Ticks und Z�hlen Nuklidimpulse Kanal1
#if !NOFIFO
    fifoInitTmIntvl(&tickFifoBuffer);
#endif
    // Timer A1 f�r Z�hlen Nuklidimpulse

    P1DIR &= ~0x40; // P1.6 = Input
    P1SEL |= 0x40;  // P1.6 = TA1CLK
    // P1SEL2 |= 0x40; //
    TA1R = 0; // Z�hler initialisieren
    TA1CCR0 = 0xFFFF;
    TA1CCR1 = 0xFFFF;
    TA1CTL =       // CNTL_0 +   // 16Bit-Counter, nicht mehr vorhanden
        TASSEL_0 + // TA1CLK as ClockSource
        ID_0 +     // Teiler 1
        MC_1;      // Timer-Mode = up
}

void initTimerA2(void) {
// 01.11.10, RL
// Timer B f�r Ausgabe Ticks und Z�hlen Nuklidimpulse
#if !NOFIFO
    fifoInitTmIntvl(&tickFifoBuffer);
#endif

    // Timer A2 f�r Z�hlen Nuklidimpulse

    P2DIR &= ~0x04; // P2.2 = Input
    P2SEL |= 0x04;  // P2.2 = TA2CLK
    // P2SEL2 |= 0x04; //
    TA2R = 0;
    TA2CCR0 = 0xFFFF;
    TA2CCR1 = 0xFFFF;
    TA2CTL =       // CNTL_0 +   // 16Bit-Counter
        TASSEL_0 + // TBCLK as ClockSource
        ID_0 +     // Teiler 1
        MC_1;      // Timer-Mode = up
}

uint8_t loadTicksFromFifoToTimerA2(void) { // Aufruf aus Timerinterrupt
    return 0;
}

uint8_t loadTicksFromFifoToTimerA1(void) { // Aufruf aus Timerinterrupt
    return 0;
}
