/*
 * main_sonde.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include <stdio.h>
#include <assert.h>
#include "../main_common.h"

void main_init(void) {
	//onShutdownProbe();     // notwendig um DAC einzuschalten
	//onBluetooth();         // notwendig um DAC einzuschalten
    //_delay(1000000);		// Pause zum hochfahren der Spannung
	initDAC(); // DAC initialisieren (I2C initialisieren)
}

void main_send_version(void) {
	const int BUFFER_LENGTH = 200;
	char buffer[BUFFER_LENGTH];
	snprintf(buffer, BUFFER_LENGTH, g_Ident, g_stInfo.strVersion,
			g_stInfo.strSerial);
	Data_send_null_terminated_string(uart_com0, buffer);
}

void main_loop(void) {
	assert(IS_PROBE);

	// 17.12.2010, RL:
	// Sondenmodul startet nach Spannungszufuhr (Batteriewechsel)
	// im Modus "Sonde AUS"
	loopProbeOffline();                // Sender an Sonde, Funktion in threads.c
	Data_send_null_terminated_string(uart_com0, g_Type1); // ASCII-String senden, Funktion in messages.c

	// Hauptschleife Sender
	do {
		if (loopProbeNormal() == 0) { // wenn Anweisung zum Abschalten der Sonde, Funktion in threads.c
			loopProbeOffline();  // dann Sonde abschalten, Funktion in threads.c
			continue;
		}
		if (loopProbeSleep() == 0) { // Funktion in threads.c
			continue;
		}
		loopProbeOffline(); // Funktion in threads.c
	} while (1);
}
