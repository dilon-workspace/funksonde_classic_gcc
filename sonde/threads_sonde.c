//-----------------------------------------------------------------------
// threads.c
//
// Arbeitsf�den
//  - F�r jeden Arbeitsmodus und Sender/Empf�nger ist ein Faden definiert:
//    * Sender - Normalbetrieb
//    * Sender - Stromsparmode
//    * Sender - Sonde aus
//    * Empf�nger - Normalbetrieb
//    * Empf�nger - Empf�nger aus (Kann eigentlich entfernt werden)
//
//-----------------------------------------------------------------------

#include <msp430.h>
#include <stdio.h>
#include <string.h>
#include "../main_common.h"
#include "../messages_common.h"
#include "main_spezial.h"

// Bit 0 = Low Bat
// Bit 1 = Min Bat
// Bit 2 = Min 3.3V
// Bit 3 = Min 5V
// Bit 4 = Min -5V
// Bit 5 = Min -31V
// Bit 6 = Test Bat

// Sender an Sonde - Normalbetrieb
int loopProbeNormal(void) {

	uint16_t wSec = 0;
	uint16_t wADC = 0;
	uint16_t wLB = 0;
	uint16_t wBT = 0;
	uint8_t ucTick = 0;
	uint16_t wTickA1 = 0;
	uint16_t wTickA2 = 0;

	uint16_t wLastGreat = g_wSecTimer;

	g_bBTRLast = false;
	g_ucModus = board_mode_probe_normal;

	initTimerA1();
	initTimerA2();
	powerOn();
	onMeasBat();

	do {
		// Sondensignal
		checkTimerA12();
		if (g_ucMeasure) {
			Data_send_2_value(MSG_TICKS, g_wOldMeasureA1, g_wOldMeasureA2); // erstmal ausklammern

			g_ucMeasure = 0;
			// Signal �berwachen und evtl. in den Schlafmodus wechseln,
			wTickA1 += g_wOldMeasureA1;
			wTickA2 += g_wOldMeasureA2;
			ucTick++;
			if (ucTick > 1) {
				if ((!(g_wErrVoltage & 0x40)) > 0) {
					if (wTickA1 >= LOWCOUNTS) // Pr�fen der Werte mit normalen Kanal im Dualmode
						wLastGreat = g_wSecTimer;

					if (wLastGreat + SWITCHSLEEP < g_wSecTimer) {
						return 1;
					}
				}
				ucTick = 0;
				wTickA1 = 0;
				wTickA2 = 0;
			}
		}

		// ####################################################
		// 27.08.2010, RL
		// StartUpDelay um dem System Anlaufzeit zu geben
		// ####################################################
		if (g_bMSStartUpFlag == 0) {
			if (g_wMSStartUpTime < g_wMSTimer)
				g_bMSStartUpFlag = 1;
		}

		// Serielle Schnittstelle auswerten
		DataReceivedCheck(); // Funktion in messages.c

		// Batteriespannung
		// Jede Sekunde ADC-Konvertierung starten
		// if (wSec!=g_wSecTimer)
		if ((g_wSecTimer % 5) == 0 && wSec != g_wSecTimer) {
			wSec = g_wSecTimer;
			startADC();
		}

		// Wenn Konvertierung fertig, Mittelwert bilden
		if (g_ucADCCalcStart == true) {
			calcADC();
		}
		// Wenn neue ADC-Werte vorliegen
		if (wADC != g_pwMSADC[4]) {
			wADC = g_pwMSADC[4];

			g_wErrVoltage &= ~0x7F; // Spannungsfehler status r�cksetzen vor neuer Messung
			if (g_pwADC[0] < UBAT2ADC(LOWUBAT)) {
				g_wErrVoltage |= 0x01;

			}
			//#####################################################
			// 27.08.2010, RL
			// Verz�gerung f�r Start-Up-Sequenz
			//#####################################################
			if (g_bMSStartUpFlag) // Messung erst nach Ablauf Einschaltverz�gerung
			{
				//if (g_pwADC[0] < UBAT2ADC(LOWUBAT)) { // Batteriefehler noch beschleunigen
				//g_wErrVoltage |= 0x01;
				//}
				if (g_pwADC[0] > UBAT2ADC(TESTUBAT))
					g_wErrVoltage |= 0x40;
				if (g_pwADC[1] < P5V2ADC(MINP5V))
					g_wErrVoltage |= 0x08;
				if (g_pwADC[2] > N5V2ADC(MINN5V))
					g_wErrVoltage |= 0x10;
				if (g_pwADC[3] > N31V2ADC(MINN31V))
					g_wErrVoltage |= 0x20;
			}
			// 05.01.2011, RL
			// wenn Batteriespannung < 2,6V, dann Sonde ausschalten
			if (g_pwADC[0] < UBAT2ADC(OFFUBAT)) {
				while (1) {
					loopProbeOffline();

				}
			}

#ifdef STATUS8LED
            P2OUT = (g_wErrVoltage << 1) + ((~g_ucRecState) & 0x01);
#endif

#ifndef STATUS8LED
			// 15.02.2011, RL:
			// Ausgabe Verbindungsstatus an P2.0
			if (g_ucRecState & 1) {
				clearBluetoothConnect(); // Verbindung getrennt
			} else {
				setBluetoothConnect();
			}
#endif

#ifdef DEBUG_
            Data_send_2_value(MSG_PROBESTAT, g_wErrVoltage, 0); // Sondenzustand senden
#endif
		}

		// LowBat-Anzeige
		if (wLB != g_wSecTimer) {

			//onLowBatLED();
			wLB = g_wSecTimer;
			if ((g_wErrVoltage & 0x38) > 0) {
				switchLowBatLED();
			} else {
				if ((g_wErrVoltage & 0x01) > 0) {
					onLowBatLED();
				} else {
					offLowBatLED();
				}
			}
		}
		// Verbindungspr�fung
		if (g_wBTSLast > CHECKLINEINTERVAL) {
			if (g_bBTRLast == false) {
				g_wBTRLast = CHECKLINETIMEOUT + 1; // g_wSecTimer-CHECKLINETIMEOUT-1;
				g_bBTRLast = true;
			}
			Data_send_2_value(MSG_CHKLN, 0, 0);

			g_wBTSLast = 0;
		}

		if (wBT != g_wSecTimer) {
			wBT = g_wSecTimer;
			if (g_bBTRLast == false || g_wBTRLast > CHECKLINETIMEOUT) {
				switchStateLED();
				g_ucRecState |= 0x01;
			} else {
				onStateLED();
				g_ucRecState &= ~0x01;
			}

#ifdef STATUS8LED
            P2OUT = (g_wErrVoltage << 1) + ((~g_ucRecState) & 0x01);
#endif

#ifndef STATUS8LED
			// 15.02.2011, RL:
			// Ausgabe Verbindungsstatus an P2.0
			if (g_ucRecState & 1) {
				clearBluetoothConnect(); // Verbindung getrennt
			} else {
				setBluetoothConnect();
			}
#endif
		}
		if (g_ucOnOffDown == 1 && g_wOnOfffort > 2000) {
			offStateLED();
		}
	} while (!(g_ucOnOff == 1 && g_wOnOff > 2000));
	// so lange
	Data_send_2_value(MSG_TICKS, 0, 0); // Setze das SG04 auf 0 cps
	while (UCA0IE & UCTXIE) {
	}; // und warte bis es verschickt ist
	Delay_ms(200);
	g_ucOnOff = 0;
	g_ucRecState |= 0x01;
	return 0;
}

// Sender an Sonde - Stromsparmode  // �berlegen ob es grunds�tzlich eliminiert werden soll
int loopProbeSleep(void) {

	return 1; // Sonde schaltet sich ab
	/*
	 uint16_t wSec = 0;
	 uint16_t wADC = 0;
	 uint16_t wLB = 0;
	 uint8_t ucTick = 0;
	 uint16_t wTickA1 = 0;
	 // uint16_t wTickA2 = 0;
	 uint16_t wLastGreat = g_wSecTimer;

	 g_ucModus = MODE_PROBE_SLEEP;
	 powerSleep(1);
	 do {
	 // Sondensignal
	 checkTimerA12();
	 if (g_ucMeasure) {
	 g_ucMeasure = 0;
	 // Signal �berwachen und evtl. in den Normal/Offline wechseln
	 wTickA1 += g_wOldMeasureA1;
	 ucTick++;
	 if (ucTick > 1) {
	 if (wTickA1 >= LOWCOUNTS) {
	 return 0;
	 }
	 if (wLastGreat + SWITCHOFFLINE < g_wSecTimer) {
	 return 1;
	 }
	 ucTick = 0;
	 wTickA1 = 0;
	 // wTickA2 = 0;
	 }
	 }
	 // ####################################################
	 // 27.08.2010, RL
	 // StartUpDelay um dem System Anlaufzeit zu geben
	 // ####################################################
	 if (g_bMSStartUpFlag == 0) {
	 if (g_wMSStartUpTime < g_wMSTimer)
	 g_bMSStartUpFlag = 1;
	 }
	 // Batteriespannung
	 // Jede Sekunde ADC-Konvertierung starten
	 // if (wSec!=g_wSecTimer)
	 if ((g_wSecTimer % 5) == 0 && wSec != g_wSecTimer) {
	 wSec = g_wSecTimer;
	 startADC();
	 }

	 // Wenn Konvertierung fertig, Mittelwert bilden
	 if (g_ucADCCalcStart == true) {
	 calcADC();
	 }

	 // Wenn neue ADC-Werte vorliegen
	 if (wADC != g_pwMSADC[4]) {
	 wADC = g_pwMSADC[4];
	 g_wErrVoltage &= ~0x7F;

	 if (g_pwADC[0] < UBAT2ADC(LOWUBAT)) {
	 g_wErrVoltage |= 0x01;
	 }
	 //#####################################################
	 // 27.08.2010, RL
	 // Verz�gerung f�r Start-Up-Sequenz
	 //#####################################################
	 if (g_bMSStartUpFlag) {
	 if (g_pwADC[1] < P5V2ADC(MINP5V)) {
	 g_wErrVoltage |= 0x08;
	 }
	 if (g_pwADC[2] > N5V2ADC(MINN5V)) {
	 g_wErrVoltage |= 0x10;
	 }
	 if (g_pwADC[3] > N40V2ADC(MINN40V)) {
	 g_wErrVoltage |= 0x20;
	 }
	 }

	 // 05.01.2011, RL
	 // wenn Batteriespannung < 2,6V, dann Sonde ausschalten
	 if (g_pwADC[0] < UBAT2ADC(OFFUBAT)) {
	 while (1) {
	 loopProbeOffline();
	 }
	 }

	 #ifdef STATUS8LED
	 P2OUT = g_wErrVoltage << 1;
	 #endif
	 }

	 // LowBat-Anzeige
	 if (wLB != g_wSecTimer) {
	 wLB = g_wSecTimer;
	 if ((g_wErrVoltage & 0x38) > 0) {
	 switchLowBatLED();
	 } else {
	 if ((g_wErrVoltage & 0x01) > 0) {
	 onLowBatLED();
	 } else {
	 offLowBatLED();
	 }
	 }
	 }
	 // Standbysignal
	 if (wSec != g_wSecTimer) {
	 wSec = g_wSecTimer;
	 onStateLED();
	 Delay_ms(1);
	 offStateLED();
	 }
	 }

	 while (!(g_ucOnOff == 1 && g_wOnOff > 10));
	 g_ucOnOff = 0;
	 return 0;*/
}

// Sender an Sonde - Sonde aus
int loopProbeOffline(void) {
	g_ucModus = board_mode_probe_offline;

	// Alles stilllegen
	P2OUT = 0; // bedeutete urspr�nglich connect (Empf�nger) und BSL auf 0
	P3OUT &= ~0x04;

	powerOff();
	offRS232();
	offADC();
	offDAC();    // DAC in Power down mode schalten
	offXT2Clock();
	offMeasBat();
	// Auf sparenden Clock schalten
	LPMClock();
	// LPM4 aktivieren
	_BIS_SR(OSCOFF + CPUOFF + SCG0 + SCG1 + GIE);

	// Erstmal mit Standard-Clock arbeiten
	defaultClock();

	// Hardware wieder einschalten
	onMeasBat();
	onADC();
	onXT2Clock();
	onRS232();
	//onShutdownProbe();     // notwendig um DAC einzuschalten
	//onBluetooth();         // notwendig um DAC einzuschalten
	//_delay(1000000);		// Pause zum erneuten hochfahren der Spannung
	onDAC();

	g_ucOnOff = 0;
	// g_wMSStartUpTime=g_wMSTimer+30000;
	g_wMSStartUpTime = g_wMSTimer + 11000;
	g_bMSStartUpFlag = 0;
	g_wErrVoltage = 0;

	return 0;
}

bool checkPower(uint8_t ucVCheckmode) {
	int n, nCnt = ADCSIZE + 1;
	int c, cCnt = ADCSIZE + 1;

	if (ucVCheckmode == 1) {
		nCnt = 1;
		for (n = 0; n < nCnt; n++) {
			startADC();
			while (g_ucADCCalcStart == false) {
			};

			for (c = 0; c < cCnt; c++) {
				calcADC();
			}
		}
	}
	if (ucVCheckmode == 2) {
		for (n = 0; n < nCnt; n++) {
			startADC();
			while (g_ucADCCalcStart == false) {
			};
			calcADC();
		}
	}
	if (ucVCheckmode == 3) {
		nCnt = 1;
		for (n = 0; n < nCnt; n++) {
			startADC();
			while (g_ucADCCalcStart == false) {
			};
			calcADC();
		}
	}
	if (g_pwADC[0] >= UBAT2ADC(MINUBAT)) {
		return true;
	}

	return false;
}
