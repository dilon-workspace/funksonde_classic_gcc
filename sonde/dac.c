//-----------------------------------------------------------------------
// dac.c
//
// Erzeugung von Spannungen aus Digialwerten
//  - Ansprechen des externen DAC über I2C0
//  - Erzeugung der Nuklidschwellspannungen
//
//-----------------------------------------------------------------------
#include "../main_common.h"
#include "../msp430_i2c.h"

static const uint8_t DACADRESSE = 0x60; // 1100 0000 -> 1100 device code, 000 factory adress, 0 write order
static const uint8_t DACCONFIG1 = 0x50; // 01010 - sequential write command. 00 - write from ch A - ch D, 0 - UDAC
static const uint8_t DACCONFIG2 = 0x90; // 1001 0000 -> 1 00 1 internal voltage reference, normal mode at power down, gain 2x,0000 databits
static const uint8_t DACCONFIG3 = 0xD0; // 1101 0000 -> 1 10 1 internal voltage reference, 100k at power down, gain 2x,0000 databits

//#############################################################################
// globale Funktionen
//#############################################################################

void initDAC(void) { // bedeutet Initialisierung der I2C-Verbindung zum externen DAC
	P3DIR |= 0x03;   // P3.0 & P3.1 out
	P3SEL |= 0x03;   // P3.0 & P3.1 I/O Functions
	P3REN &= ~0x03;  // P3.0 & P3.1 kein Pu/Pd Resistor
	msp430_i2c_init();
}

void onDAC(void) { // DAC ist zu überarbeiten da extern
	setDAC(UTHR_RESET, UTHR_RESET, UTHR_RESET, UTHR_RESET);
}

void offDAC(void) {// DAC wird in den Powerdownmode geschaltet um Strom zu sparen
	setDACPD(UTHR_RESET, UTHR_RESET, UTHR_RESET, UTHR_RESET);
}

void setDAC(uint16_t w1, uint16_t w2, uint16_t w3, uint16_t w4) { // DAC ist zu überarbeiten da extern
	uint8_t buffer[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	uint8_t high, low;

	buffer[0] = DACCONFIG1;

	low = w1 & 0xFF;
	high = w1 >> 8;

	buffer[1] = high;
	buffer[1] |= DACCONFIG2; // VerODERn mit Grundkonfiguration

	buffer[2] = low;

	low = w2 & 0xFF;
	high = w2 >> 8;

	buffer[3] = high;
	buffer[3] |= DACCONFIG2; // VerODERn mit Grundkonfiguration

	buffer[4] = low;

	low = w3 & 0xFF;
	high = w3 >> 8;

	buffer[5] = high;
	buffer[5] |= DACCONFIG2; // VerODERn mit Grundkonfiguration

	buffer[6] = low;

	low = w4 & 0xFF;
	high = w4 >> 8;

	buffer[7] = high;
	buffer[7] |= DACCONFIG2; // VerODERn mit Grundkonfiguration

	buffer[8] = low;

	msp430_i2c_send_buffer(DACADRESSE, sizeof(buffer), buffer);
}

void setDACPD(uint16_t w1, uint16_t w2, uint16_t w3, uint16_t w4) { // DAC ist zu überarbeiten da extern
	uint8_t buffer[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	uint8_t high, low;

	buffer[0] = DACCONFIG1;

	low = w1 & 0xFF;
	high = w1 >> 8;

	buffer[1] = high;
	buffer[1] |= DACCONFIG3; // VerODERn mit Grundkonfiguration

	buffer[2] = low;

	low = w2 & 0xFF;
	high = w2 >> 8;

	buffer[3] = high;
	buffer[3] |= DACCONFIG3; // VerODERn mit Grundkonfiguration

	buffer[4] = low;

	low = w3 & 0xFF;
	high = w3 >> 8;

	buffer[5] = high;
	buffer[5] |= DACCONFIG3; // VerODERn mit Grundkonfiguration

	buffer[6] = low;

	low = w4 & 0xFF;
	high = w4 >> 8;

	buffer[7] = high;
	buffer[7] |= DACCONFIG3; // VerODERn mit Grundkonfiguration

	buffer[8] = low;

	msp430_i2c_send_buffer(DACADRESSE, sizeof(buffer), buffer);
}
