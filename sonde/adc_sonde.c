/*
 * adc.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#include <msp430.h>

void adc_init_port(void) {
	P6DIR &= ~0x1F; // P6.0-P6.4 als Input in Sonde
	P6SEL |= 0x1F;  // ADC-Function
	P6REN &= ~0x1F; // P6.0-P6.4 ohne PullUpDown
}

void adc_init_sequence(void) {
	ADC12MCTL0 = ADC12INCH_0 + ADC12SREF_1;            // UBAT
	ADC12MCTL1 = ADC12INCH_1 + ADC12SREF_1;            //+5V
	ADC12MCTL2 = ADC12INCH_2 + ADC12SREF_1;            //-5V
	ADC12MCTL3 = ADC12INCH_3 + ADC12SREF_1;            //-40
	ADC12MCTL4 = ADC12INCH_4 + ADC12SREF_1 + ADC12EOS; //+3.3V
	ADC12IE = 0x001F;
}
