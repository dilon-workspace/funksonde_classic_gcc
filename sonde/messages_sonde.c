/*
 * messages_sonde.c
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */
#include <string.h>
#include <stdio.h>

#include "../messages_common.h"
#include "../main_common.h"
#include "../info.h"

char g_Type1[] = "Sendemodul\r\n"
                 "----------------------------------------------------\r\n";

char g_PowerSleep[] = "Power SLEEP\r\n";

void parse_received_message(strMESSAGE g_stMsgRec) {
    // Messdaten Nuklid
    uint16_t w = 0;

    if (g_stMsgRec.ucType == MSG_NUKLID) {
        memcpy(&w, &g_stMsgRec.ucData[0], sizeof(uint16_t));
        if (w > 0 && w < NUKLIDMAX) {
            //###################################################
            // 27.08.2010, RL
            // Verz�gerung f�r Strat-Up-Sequenz im SG03-Modus Sequenzen f�r SG03 h�chstwahrscheinlich entfernen
            //###################################################
            if (g_bMSStartUpFlag) {
                setDAC(g_stInfo.pwTableTra[w - 1][0], g_stInfo.pwTableTra[w - 1][1], g_stInfo.pwTableTra[w - 1][2], g_stInfo.pwTableTra[w - 1][3]);
                if (g_stMsgRec.ucData[2] == 0) { // MT101018
                    Data_send_3_value(uart_com0, MSG_CMDOK, 0, 0, 0);
                }
            } else {
                if (g_stMsgRec.ucData[2] == 0) { // MT101018
                    Data_send_3_value(uart_com0, MSG_CMDERR, 0, 0, 0);
                }
            }
        } else {
            setDAC(UTHR_RESET, UTHR_RESET, UTHR_RESET, UTHR_RESET);
            if (g_stMsgRec.ucData[2] == 0) {
                Data_send_3_value(uart_com0, MSG_CMDERR, 0, 0, 0); // MT101018
            }
        }
    }

    // Nachfolgend ist noch zu pr�fen ob Nukliderweiterung hier eingepflegt werden mu�

    // Nuklid direkt ausgeben
    if (g_stMsgRec.ucType == MSG_OUTNUKLID) {
        uint16_t dacval1 = 0;
        uint16_t dacval2 = 0;
        memcpy(&dacval1, &g_stMsgRec.ucData[0], sizeof(uint16_t));
        memcpy(&dacval2, &g_stMsgRec.ucData[2], sizeof(uint16_t));
        Data_send_3_value(uart_com0, MSG_CMDOK, 0, 0, 0); // MT101018
    }

    // Ausleseanfrage
    if (g_stMsgRec.ucType == MSG_GETCMD) {
        memcpy(&w, &g_stMsgRec.ucData[0], sizeof(uint16_t));
        if (w == 1) {
            Data_send_string(uart_com0, MSG_GETVERSION, (uint8_t *)&g_stInfo.strVersion, 12);
        } else {
            if (w == 2) {
                Data_send_string(uart_com0, MSG_GETSERIAL, (uint8_t *)&g_stInfo.strSerial, 12);
            } else {
                if (w == 3) {
                    Data_send_string(uart_com0, MSG_GETNAME, (uint8_t *)&g_stInfo.strName, 12);
                } else {
                    if (w == 4) {
                        memcpy(&w, &g_stMsgRec.ucData[2], sizeof(uint16_t));
                        // w = *((uint16_t *)&g_stMsgRec.ucData[2]);
                        if (w > 0 && w < NUKLIDMAX) {
                            Data_send_3_value(uart_com0, MSG_GETNUKLID, w, g_stInfo.pwTableTra[w - 1][0], g_stInfo.pwTableTra[w - 1][1]);
                        } else {
                            Data_send_3_value(uart_com0, MSG_CMDERR, 0, 0, 0); // MT101018
                        }
                    }
                }
            }
        }
    }

    // Nuklideintrag schreiben
    if (g_stMsgRec.ucType == MSG_SETNUKLID) {
        // w = *((uint16_t *)g_stMsgRec.ucData);
        memcpy(&w, &g_stMsgRec.ucData[0], sizeof(uint16_t));
        if (w > 0 && w < NUKLIDMAX) {
            strINFO g_stInf = {0};
            memcpy(&g_stInf, &g_stInfo, sizeof(strINFO));
            uint16_t value1 = 0;
            uint16_t value2 = 0;
            uint16_t value3 = 0;
            uint16_t value4 = 0;
            memcpy(&value1, &g_stMsgRec.ucData[2], sizeof(uint16_t));
            memcpy(&value2, &g_stMsgRec.ucData[4], sizeof(uint16_t));
            // g_stInf.pwTableTra[w - 1][0] = ((uint16_t *)g_stMsgRec.ucData)[1];
            // g_stInf.pwTableTra[w - 1][1] = ((uint16_t *)g_stMsgRec.ucData)[2];
            g_stInf.pwTableTra[w - 1][0] = value1;
            g_stInf.pwTableTra[w - 1][1] = value2;
            writeInfo(&g_stInf);
            Data_send_3_value(uart_com0, MSG_CMDOK, 0, 0, 0); // MT101018
        } else {
            Data_send_3_value(uart_com0, MSG_CMDERR, 0, 0, 0); // MT101018
        }
    }
}
