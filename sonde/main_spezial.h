/*
 * main_spezial.h
 *
 *  Created on: 21.01.2020
 *      Author: ak
 */

#ifndef SONDE_MAIN_SPEZIAL_H_
#define SONDE_MAIN_SPEZIAL_H_

typedef enum { board_mode_probe_normal = 1, board_mode_probe_sleep = 2, board_mode_probe_offline = 3 } board_mode_t;

#define IS_PROBE 1 // 0=Empf�nger; 1=Sender

#endif /* SONDE_MAIN_SPEZIAL_H_ */
