/*
 * io_sonde.c
 *
 *  Created on: 21.01.2020 BTWINDOW
 *      Author: ak
 */

#include <stdint.h>
#include "../main_common.h"

// Sonde einschalten
void powerOn(void) {
	uint16_t wMS = 0;
	bool bon = false;

	onShutdownProbe(); // P5.3 on, /Shutdown Probe on
	onBluetooth();     // P5.4 on, Bluetooth on
	onStateLED();      // P3.1 on, Status LED on

	wMS = g_wMSTimer;

	do {
		if ((wMS + POWER_ON_DELAY) < (g_wMSTimer)) {
			bon = true;
		}

		checkPower(FASTB);
		if ((g_pwADC[0] > UBAT2ADC(LOWUBAT)) && (g_pwADC[1] > P5V2ADC(LOWP5V))
				&& (g_pwADC[2] > N5V2ADC(MINN5V))
				&& (g_pwADC[3] > N31V2ADC(MINN31V))
				&& (g_pwADC[4] > P3V32ADC(MINP3V3))) {
			bon = true;
		}
	} while (bon == false);

	Data_send_null_terminated_string(uart_com0, g_PowerOn); // erstmal ausgeklammert

	// 01.11.10, RL
	startTimerA1(); // Z�hler erstmal ausgeklammert        // Z�hler starten
	startTimerA2(); // Z�hler erstmal ausgeklammert    //Z�hler starten
}

// Sonde Stromsparend schalten
void powerSleep(unsigned char ucIsTransmitter) {
	uint16_t wMS = 0;
	bool bon = false;

	Data_send_null_terminated_string(uart_com0, g_PowerSleep);
	offLowBatLED();    // P3.0 off, LowBat aus
	offStateLED();     // P3.1 off, Status aus
	onShutdownProbe(); // P5.3 on, /Shutdown Probe on
	offBluetooth();    // P5.4 off, Bluetooth off
	wMS = g_wMSTimer;
	do {
		if ((wMS + 500) < (g_wMSTimer)) {
			bon = true;
		}

		checkPower(FASTB);
		if ((g_pwADC[0] > UBAT2ADC(LOWUBAT)) && (g_pwADC[1] > P5V2ADC(LOWP5V))
				&& (g_pwADC[2] > N5V2ADC(MINN5V))
				&& (g_pwADC[3] > N31V2ADC(MINN31V))
				&& (g_pwADC[4] > P3V32ADC(MINP3V3))) {
			bon = true;
		}
	} while (bon == false);
}

// Sonde ausschalten
void powerOff(void) {
	stoppTimerA1(); // Z�hler stoppen
	stoppTimerA2();
	Data_send_null_terminated_string(uart_com0, g_PowerOff);
	offLowBatLED();     // P2.7 off, LowBat aus
	offStateLED();      // P2.6 off, Status aus
	offShutdownProbe(); // P1.7 off, /Shutdown Probe off
	offBluetooth();     // P4.3 off, Bluetooth off
}

//---------------------------------------------------------------------------
