//-----------------------------------------------------------------------
// message.c
//
// Ein- und Ausgabefunktionen
//  - Strings f�r das UI
//  - Funktionen f�r das SDM-XR-Bluetooth-Protokoll
//
//-----------------------------------------------------------------------
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main_common.h"
#include "messages_common.h"
#include "messages_spezial.h"

char g_Ident[] = "\r\n"
                 "----------------------------------------------------\r\n"
                 "SDM-XR V%s #%s\r\n"
                 "(c) SENSYS GmbH 2009-2010\r\n"
                 "----------------------------------------------------\r\n";

char g_PowerOn[] = "Power ON\r\n";
char g_PowerOff[] = "Power OFF\r\n";

const uint8_t MSG_TICKS = 0x00;      // Messwert (Z�hlerstand)
const uint8_t MSG_ADC = 0x01;        // Messwert (ADC/R�ckkanal)
const uint8_t MSG_NUKLID = 0x02;     // Messwert als Nuklidwahl (ADC/R�ckkanal)
const uint8_t MSG_CHKLN = 0x10;      // Check Line Anfrage
const uint8_t MSG_ACKLN = 0x11;      // Check Line Antwort
const uint8_t MSG_CMDERR = 0x12;     // MT101018 Command Error
const uint8_t MSG_CMDOK = 0x13;      // MT101018 Command OK
const uint8_t MSG_PROBESTAT = 0x20;  // Sondenzustand
const uint8_t MSG_GETCMD = 0x50;     // Kommando zum Auslesen
const uint8_t MSG_GETVERSION = 0x51; // Versionsnummer auslesen
const uint8_t MSG_SETVERSION = 0x52; // Versionsnummer setzen
const uint8_t MSG_GETSERIAL = 0x53;  // Seriennummer auslesen
const uint8_t MSG_SETSERIAL = 0x54;  // Seriennummer setzen
const uint8_t MSG_GETNAME = 0x55;    // Sondenname auslesen
const uint8_t MSG_SETNAME = 0x56;    // Sondenname setzen
const uint8_t MSG_GETNUKLID = 0x57;  // Nuklidpaar auslesen
const uint8_t MSG_SETNUKLID = 0x58;  // Nuklidpaar setzen
const uint8_t MSG_OUTNUKLID = 0x59;  // Nuklidpaar ausgeben

uint8_t g_ucMsgSend = 0; // MessageSendez�hler
// strMESSAGE g_stMsgSend;                // Sendemessage
uint8_t g_ucMsgRec = 0;                // Empfange Bytes
strMESSAGE g_stMsgRec;                 // letzte Empfangsmessage
char *g_pMsgRec = (char *)&g_stMsgRec; // Pointer auf Empfangsmessage

uint8_t g_ucTransFlags = 0; // Flags des Empf�ngers (aktualisiert mit jeder Message)

void parse_received_message(strMESSAGE g_stMsgRec);

void Data_send_null_terminated_string(uart_com_index_t ucPort, const char *data) {
    sendRS232(ucPort, (unsigned char *)data, strlen(data));
}

void DebugSendEx(uart_com_index_t ucPort, char *data) {
#ifdef _DEBUG
    sendRS232(ucPort, (unsigned char *)data, strlen(data));
#endif
}

uint8_t getChecksumme(uint8_t *p) {
    uint8_t b = 0;

    if (p == NULL) {
        return 0;
    }
    for (size_t n = 1; n < sizeof(strMESSAGE) - 2; n++) {
        b += p[n];
    }
    return b;
}

void Data_send_2_value(uint8_t ucType, uint16_t wValue1, uint16_t wValue2) {
    Data_send_3_value(uart_com0, ucType, wValue1, wValue2, 0);
}

void Data_send_3_value(uart_com_index_t ucPort, uint8_t ucType, uint16_t wValue1, uint16_t wValue2, uint16_t wValue3) {
    strMESSAGE g_stMsgSend; // Sendemessage
    memset(&g_stMsgSend, 0, sizeof(strMESSAGE));
    g_stMsgSend.ucStart = 0x02;
    g_stMsgSend.ucType = ucType;
    g_stMsgSend.ucCount = g_ucMsgSend++;
    // MT101018 Bug: Status <0x04 wurde immer als 0x04 ausgegeben. Grund???
    // g_stMsgSend.ucFlags=(g_wErrVoltage&0x03)+((g_wErrVoltage&0x3C)>0)?0x04:0x00;
    g_stMsgSend.ucFlags = (g_wErrVoltage & 0x03);

    if ((g_wErrVoltage & 0x3C) > 0) {
        g_stMsgSend.ucFlags += 0x04;
    }

    memcpy(&g_stMsgSend.ucData[0], &wValue1, sizeof(uint16_t));
    memcpy(&g_stMsgSend.ucData[2], &wValue2, sizeof(uint16_t));
    memcpy(&g_stMsgSend.ucData[4], &wValue3, sizeof(uint16_t));

    g_stMsgSend.ucChks = getChecksumme((uint8_t *)&g_stMsgSend);
    g_stMsgSend.ucStopp = 0x03;

    sendRS232(ucPort, (unsigned char *)&g_stMsgSend, sizeof(strMESSAGE));
}

void Data_send_string(uart_com_index_t ucPort, uint8_t ucType, uint8_t *pB, uint8_t bLength) {
    strMESSAGE g_stMsgSend; // Sendemessage
    if (bLength > 12)
        return;

    memset(&g_stMsgSend, 0, sizeof(strMESSAGE));
    g_stMsgSend.ucStart = 0x02;
    g_stMsgSend.ucType = ucType;
    // MT101018 Bug: Status <0x04 wurde immer als 0x04 ausgegeben. Grund???
    // g_stMsgSend.ucFlags=(g_wErrVoltage&0x03)+((g_wErrVoltage&0x3C)>0)?0x04:0x00;
    g_stMsgSend.ucFlags = (g_wErrVoltage & 0x03);

    if ((g_wErrVoltage & 0x3C) > 0)
        g_stMsgSend.ucFlags += 0x04;

    g_stMsgSend.ucCount = g_ucMsgSend++;
    memcpy(&g_stMsgSend.ucData, pB, bLength);
    g_stMsgSend.ucChks = getChecksumme((uint8_t *)&g_stMsgSend);
    g_stMsgSend.ucStopp = 0x03;

    sendRS232(ucPort, (unsigned char *)&g_stMsgSend, sizeof(strMESSAGE));
}

bool DataReceivedCheck(void) {
    char c;
    int o;
    bool bRet = false;
    bool bRead = true;

    while (bRead) {                                               // neue(s) Zeichen im Puffer
        bRead = receiveByteRS232(uart_com0, (unsigned char *)&c); // neues Zeichen holen

        if (!bRead) {
            continue;
        }
        if (g_ucMsgRec == 0) {
            if (c != 0x02) {
                continue;
            }
            g_pMsgRec[g_ucMsgRec++] = c;
        }
        if (!bRead) {
            continue;
        }
        if (g_ucMsgRec == 0) {
            if (c != 0x02)
                continue;
            g_pMsgRec[g_ucMsgRec++] = c;
        } else {
            g_pMsgRec[g_ucMsgRec++] = c;
            if (g_ucMsgRec >= sizeof(strMESSAGE)) {
                if ((g_stMsgRec.ucStart == 0x02) && (g_stMsgRec.ucChks == getChecksumme((uint8_t *)&g_stMsgRec)) && (g_stMsgRec.ucStopp == 0x03)) {
                    parse_received_message(g_stMsgRec);

                    // Messages f�r beide Seiten
                    // Verbindungsnachfrage
                    if (g_stMsgRec.ucType == MSG_CHKLN) {
                        Data_send_2_value(MSG_ACKLN, 0, 0);
                    }
                    // Verbindung best�tigt
                    if (g_stMsgRec.ucType == MSG_ACKLN) {
                        g_wBTRLast = 0;
                        g_bBTRLast = true;
                    }

                    // Senderstatus merken
                    g_ucTransFlags = g_stMsgRec.ucFlags;
                    g_ucMsgRec = 0;
                    memset(&g_stMsgRec, 0, sizeof(g_stMsgRec));

                    bRet = true;
                    continue;
                } else { // Message fehlerhaft
                    o = 0;

                    for (size_t n = 1; n < sizeof(strMESSAGE); n++) {
                        if (g_pMsgRec[n] == 0x02) {
                            for (size_t m = n; m < sizeof(strMESSAGE); m++) {
                                g_pMsgRec[m - n] = g_pMsgRec[m];
                            }
                            o = g_ucMsgRec - n;
                            break;
                        }
                    }
                    g_ucMsgRec = o;
                    continue;
                }
            }
        }
    }
    return bRet;
}
