
//-----------------------------------------------------------------------
// adc.c
//
// Digitalisierung analoger Spannugen
//  - Versorgungsspannungen
//  - Nuklidwahlspannung
//
//-----------------------------------------------------------------------
#include "main_common.h"
#include <string.h>

uint16_t g_pwADC[8];   // ADC-Samples
uint16_t g_pwMSADC[8]; // ADC-Sample-Zeiten (MS)

uint16_t pwADCZ[8];
uint16_t pwADCsum[8];
uint16_t pwADCbuffer[8][ADCSIZE];
uint16_t pwADCbufcnt;
volatile bool g_ucADCCalcStart = false;

uint16_t g_wCh; // von ADC_IR

void adc_init_port(void);
void adc_init_sequence(void);
//#############################################################################
// Interner AD-Wandler
//#############################################################################

//*****************************************************************************
void initADC(void) {
    uint8_t a, b, c; // f�r initialisierung pwADCbuffer
    memset(g_pwADC, 0, 16);
    memset(g_pwMSADC, 0, 16);
    memset(pwADCbuffer, 0, 8 * ADCSIZE * 2);
    memset(pwADCsum, 0, 16);
    memset(pwADCZ, 0, 16);
    pwADCbufcnt = 0;

    adc_init_port();

    REFCTL0 = REFMSTR + REFVSEL_0 + REFON;
    ADC12CTL0 = ADC12SHT0_5 +      // Sample&Hold: 96Zylken
                ADC12MSC +         // Multiple sample and conversion
                ADC12REFON +       // Referenzspannung 1,5V ein
                ADC12ON;           // ADC ein
    ADC12CTL1 = ADC12CSTARTADD_0 + // Start mit Kanal 0
                ADC12SHS_0 +       // Sample&Hold Source ADC12SCbit
                ADC12SHP +         // Sample&Hold Pulse Mode
                ADC12DIV_7 +       // Teiler 8
                ADC12SSEL_3 +      // SMCLK-Source
                ADC12CONSEQ_1;     // Konvertierungsmode: Kanal-Sequentiell
                                   // diese Einstellungen gegf. pr�fen
    ADC12CTL2 = ADC12TCOFF +       // // Predivider 1, Temperatursensor aus
                ADC12RES_2 +       // 12 Bit resolution
                ADC12SR;           // 50k2ps

    adc_init_sequence();
    ADC12IFG = 0; // ggf. anh�ngige Interrupts l�schen
}

void onADC(void) {
    ADC12CTL0 |= ADC12REFON + ADC12ON + ADC12ENC; // ADC+Referenz wieder zuschalten
}

void offADC(void) {

    ADC12CTL0 &= ~ADC12SC;    // Keine Konvertierung mehr starten
    ADC12CTL0 &= ~ADC12ENC;   // Encodern stoppen
    ADC12CTL0 &= ~ADC12ON;    // ADC abschalten
    ADC12CTL0 &= ~ADC12REFON; // Referenzspannung abschalten
}

void startADC(void) {
    ADC12CTL0 |= ADC12ENC; // Conversion freigeben
    ADC12CTL0 |= ADC12SC;  // Start Sample and Conversion
}

void calcADC(void) {
    uint8_t ucADC;

    g_ucADCCalcStart = false;

    for (ucADC = 0; ucADC < 8; ucADC++) {
        pwADCsum[ucADC] -= pwADCbuffer[ucADC][pwADCbufcnt];
        pwADCsum[ucADC] += pwADCZ[ucADC];
        pwADCbuffer[ucADC][pwADCbufcnt] = pwADCZ[ucADC];

        g_pwADC[ucADC] = (uint16_t)(pwADCsum[ucADC] / (ADCSIZE * 1.0));
        g_pwMSADC[ucADC] = g_wMSTimer;
    }

    pwADCbufcnt++;
    if (pwADCbufcnt >= ADCSIZE)
        pwADCbufcnt = 0;
}

//*****************************************************************************
//#pragma interrupt_handler ADC_IR : ADC12_VECTOR
__attribute__((interrupt(ADC12_VECTOR))) void ADC_IR(void) {

    g_wCh = ADC12IV;

    if ((g_wCh & 0x06) > 0) {
        pwADCZ[0] = ADC12MEM0;
    }
    if ((g_wCh & 0x08) > 0) {
        pwADCZ[1] = ADC12MEM1;
    }
    if ((g_wCh & 0x0A) > 0) {
        pwADCZ[2] = ADC12MEM2;
    }
    if ((g_wCh & 0x0C) > 0) {
        pwADCZ[3] = ADC12MEM3;
    }
    if ((g_wCh & 0x0E) > 0) {
        pwADCZ[4] = ADC12MEM4;
        g_ucADCCalcStart = true;
    }
    if ((g_wCh & 0x10) > 0) {
        pwADCZ[5] = ADC12MEM5;
    }
    if ((g_wCh & 0x12) > 0) {
        pwADCZ[6] = ADC12MEM6;
    }
    if ((g_wCh & 0x14) > 0) {
        pwADCZ[7] = ADC12MEM7;
    }
}
