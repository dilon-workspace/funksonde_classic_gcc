/*
 * msp430_i2c.h
 *
 *  Created on: 17.12.2019
 *      Author: ak
 */

#ifndef MSP430_I2C_H_
#define MSP430_I2C_H_

void msp430_i2c_init(void);
void msp430_i2c_send_buffer(const uint8_t i2c_address, uint16_t buffer_length, uint8_t *buffer);

#endif /* MSP430_I2C_H_ */
