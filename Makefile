#
# Makefile for msp430
#
# 'make' builds everything
# 'make clean' deletes everything except source files and Makefile
# You need to set TARGET, MCU and SOURCES for your project.
# TARGET is the name of the executable file to be produced 
# $(TARGET).elf $(TARGET).hex and $(TARGET).txt nad $(TARGET).map are all generated.
# The TXT file is used for BSL loading, the ELF can be used for JTAG use
# 
TARGET_NAME_SONDE     = funksonde_classic_gcc_sonde
TARGET_NAME_SG04     = funksonde_classic_gcc_sg04
TARGET_NAME_DONGLE     = funksonde_classic_gcc_dongle

MCU        = msp430f5528
MSP430_GCC_PATH = C:/TI/msp430-gcc/

# List all the source files here
# eg if you have a source file foo.c then list it here



SOURCES += main.c
SOURCES += bt_conf.c
SOURCES += clock.c
SOURCES += fifo.c
SOURCES += histerese.c
SOURCES += info.c
SOURCES += io.c
SOURCES += messages.c
SOURCES += p1int.c
SOURCES += rs232.c
SOURCES += timer.c
SOURCES += adc.c
SOURCES += msp430_i2c.c
SOURCES += threads.c
SOURCES += MSP430F5xx_6xx/usci_b_i2c.c

SOURCES_SONDE += $(SOURCES)
SOURCES_SONDE += sonde/dac.c
SOURCES_SONDE += sonde/io_sonde.c
SOURCES_SONDE += sonde/main_sonde.c
SOURCES_SONDE += sonde/messages_sonde.c
SOURCES_SONDE += sonde/rs232_sonde.c
SOURCES_SONDE += sonde/threads_sonde.c
SOURCES_SONDE += sonde/timer_sonde.c
SOURCES_SONDE += sonde/adc_sonde.c

SOURCES_SG04_DONGLE += $(SOURCES)
SOURCES_SG04_DONGLE += sg04_dongle/main_sg04_dongle.c
SOURCES_SG04_DONGLE += sg04_dongle/messages_sg04_dongle.c
SOURCES_SG04_DONGLE += sg04_dongle/threads_sg04_dongle.c
SOURCES_SG04_DONGLE += sg04_dongle/timer_sg04_dongle.c
SOURCES_SG04_DONGLE += sg04_dongle/adc_sg04_dongle.c
SOURCES_SG04_DONGLE += sg04_dongle/rs232_sg04_dongle.c
SOURCES_SG04_DONGLE += sg04_dongle/io_sg04_dongle.c

SOURCES_SG04 += $(SOURCES_SG04_DONGLE)
SOURCES_SG04 += sg04/main_sg04.c
SOURCES_SG04 += sg04/soft_uart_tx_sg04.c
SOURCES_SG04 += sg04/soft_uart_fifo.c

SOURCES_DONGLE += $(SOURCES_SG04_DONGLE)
SOURCES_DONGLE += dongle/main_dongle.c

INCLUDE_SG04_DONGLE = -Isg04_dongle/

INCLUDE_SONDE = -Isonde/

INCLUDE_SG04 += $(INCLUDE_SG04_DONGLE)
INCLUDE_SG04 += -Isg04/

INCLUDE_DONGLE += $(INCLUDE_SG04_DONGLE)
INCLUDE_DONGLE += -Idongle/


LINKER_SCRIPT = $(MCU)_linkerscript.ld


# Include are located in the Include directory
INCLUDES = -IInclude
INCLUDES += -I$(MSP430_GCC_PATH)include

# Add or subtract whatever MSPGCC flags you want. There are plenty more
#######################################################################################
CFLAGS   = -mmcu=$(MCU) -g -O2 -Wall -Wunused $(INCLUDES)   
ASFLAGS  = -mmcu=$(MCU) -x assembler-with-cpp -Wa,-gstabs

LDFLAGS  = -mmcu=$(MCU) -T$(LINKER_SCRIPT)

LDFLAGS_SONDE  = $(LDFLAGS) -Wl,-Map=$(TARGET_SONDE).map
LDFLAGS_SG04   = $(LDFLAGS) -Wl,-Map=$(TARGET_SG04).map
LDFLAGS_DONGLE = $(LDFLAGS) -Wl,-Map=$(TARGET_DONGLE).map


CFLAGS += -Wall -Wchar-subscripts -Wcomment -Wformat=2 -Wimplicit-int
CFLAGS += -Werror-implicit-function-declaration -Wmain -Wparentheses
CFLAGS += -Wsequence-point -Wreturn-type -Wswitch -Wtrigraphs -Wunused
CFLAGS += -Wuninitialized -Wunknown-pragmas -Wfloat-equal -Wundef -Wno-unused-function -Wno-unused-variable
CFLAGS += -Wshadow -Wpointer-arith -Wbad-function-cast -Wwrite-strings 
CFLAGS += -Wsign-compare -Waggregate-return -Wstrict-prototypes
#CFLAGS += -Wmissing-declarations
#CFLAGS +=  -Werror 
CFLAGS += -Wformat -Wmissing-format-attribute -Wno-deprecated-declarations
#CFLAGS += -Wpacked -Wredundant-decls -Wnested-externs -Winline -Wlong-long
CFLAGS += -Wredundant-decls -Wnested-externs
CFLAGS += -Wunreachable-code
CFLAGS += -Wcast-align

CFLAGS_SONDE += $(CFLAGS)
CFLAGS_SONDE += $(INCLUDE_SONDE)

CFLAGS_SG04 += $(CFLAGS)
CFLAGS_SG04 += $(INCLUDE_SG04)

CFLAGS_DONGLE += $(CFLAGS)
CFLAGS_DONGLE += $(INCLUDE_DONGLE)

########################################################################################
SH 			= "C:\Program Files\Git\usr\bin\sh.exe"
MKDIR 		= "C:\Program Files\Git\usr\bin\mkdir.exe"
TOOLS = "C:\Program Files\Git\usr\bin\"

MSP_PREFIX = $(MSP430_GCC_PATH)bin/msp430-elf-
CC       = $(MSP_PREFIX)gcc
LD       = $(MSP_PREFIX)ld
AR       = $(MSP_PREFIX)ar
AS       = $(MSP_PREFIX)gcc
GASP     = $(MSP_PREFIX)gasp
NM       = $(MSP_PREFIX)nm
OBJCOPY  = $(MSP_PREFIX)objcopy
RANLIB   = $(MSP_PREFIX)ranlib
STRIP    = $(MSP_PREFIX)strip
SIZE     = $(MSP_PREFIX)size
READELF  = $(MSP_PREFIX)readelf
MAKETXT  = $(MSP_PREFIX)srec_cat
CP       = $(TOOLS)cp -p
RM       = $(TOOLS)rm -rf
MV       = $(TOOLS)mv
########################################################################################
# the file which will include dependencies
# all the object files

OBJDIR_SONDE = build_sonde
OBJDIR_SG04 = build_sg04
OBJDIR_DONGLE = build_dongle

OBJECTS_SONDE +=  $(addprefix $(OBJDIR_SONDE)/,$(addsuffix .o,$(basename $(SOURCES_SONDE))))
DEPENDS_SONDE  = $(addprefix $(OBJDIR_SONDE)/,$(addsuffix .d,$(basename $(SOURCES_SONDE))))

OBJECTS_SG04 +=  $(addprefix $(OBJDIR_SG04)/,$(addsuffix .o,$(basename $(SOURCES_SG04))))
DEPENDS_SG04  = $(addprefix $(OBJDIR_SG04)/,$(addsuffix .d,$(basename $(SOURCES_SG04))))

OBJECTS_DONGLE +=  $(addprefix $(OBJDIR_DONGLE)/,$(addsuffix .o,$(basename $(SOURCES_DONGLE))))
DEPENDS_DONGLE  = $(addprefix $(OBJDIR_DONGLE)/,$(addsuffix .d,$(basename $(SOURCES_DONGLE))))



TARGET_SONDE = $(OBJDIR_SONDE)/$(TARGET_NAME_SONDE)
TARGET_SG04 = $(OBJDIR_SG04)/$(TARGET_NAME_SG04)
TARGET_DONGLE = $(OBJDIR_DONGLE)/$(TARGET_NAME_DONGLE)

DEPEND_FLAGS_SONDE = -MMD -MP -MF $(OBJDIR_SONDE)/$(*D)/$(*F).d
DEPEND_FLAGS_SG04 = -MMD -MP -MF $(OBJDIR_SG04)/$(*D)/$(*F).d
DEPEND_FLAGS_DONGLE = -MMD -MP -MF $(OBJDIR_DONGLE)/$(*D)/$(*F).d

all: git_version all_sonde all_sg04 all_dongle

all_sonde:  $(TARGET_SONDE).elf $(TARGET_SONDE).hex
all_sg04: $(TARGET_SG04).elf $(TARGET_SG04).hex
all_dongle: $(TARGET_DONGLE).elf $(TARGET_DONGLE).hex

git_version:
	echo "Get Git-Version"
	$(SH) git.sh

$(TARGET_SONDE).elf: $(OBJECTS_SONDE)
	$(MKDIR) -p $(@D)
	echo "Linking $@"
	$(CC) $^ $(LDFLAGS_SONDE) $(LIBS) -o $@
	echo
	echo ">>>> Size of Firmware <<<<"
	$(SIZE) $(TARGET_SONDE).elf
	echo
	
$(TARGET_SG04).elf: $(OBJECTS_SG04)
	$(MKDIR) -p $(@D)
	echo "Linking $@"
	$(CC) $^ $(LDFLAGS_SG04) $(LIBS) -o $@
	echo
	echo ">>>> Size of Firmware <<<<"
	$(SIZE) $(TARGET_SG04).elf
	echo
	
$(TARGET_DONGLE).elf: $(OBJECTS_DONGLE)
	$(MKDIR) -p $(@D)
	echo "Linking $@"
	$(CC) $^ $(LDFLAGS_DONGLE) $(LIBS) -o $@
	echo
	echo ">>>> Size of Firmware <<<<"
	$(SIZE) $(TARGET_DONGLE).elf
	echo
	
%.hex: %.elf
	$(MKDIR) -p $(@D)
	$(OBJCOPY) -O ihex $< $@

$(OBJDIR_SONDE)/%.o: %.c
	echo "Compiling $<"
	$(MKDIR) -p $(@D)
	$(CC) -c $(CFLAGS_SONDE) $(DEPEND_FLAGS_SONDE) $< -o $@ 

$(OBJDIR_SONDE)/%.lst: %.c
	$(MKDIR) -p $(@D)
	$(CC) -c $(CFLAGS_SONDE) -Wa,-anlhd $< > $@
	
	
	
$(OBJDIR_SG04)/%.o: %.c
	echo "Compiling $<"
	$(MKDIR) -p $(@D)
	$(CC) -c $(CFLAGS_SG04) $(DEPEND_FLAGS_SG04) $< -o $@ 

$(OBJDIR_SG04)/%.lst: %.c
	$(MKDIR) -p $(@D)
	$(CC) -c $(CFLAGS_SG04) -Wa,-anlhd $< > $@
	
	
	
$(OBJDIR_DONGLE)/%.o: %.c
	echo "Compiling $<"
	$(MKDIR) -p $(@D)
	$(CC) -c $(CFLAGS_DONGLE) $(DEPEND_FLAGS_DONGLE) $< -o $@ 

$(OBJDIR_DONGLE)/%.lst: %.c
	$(MKDIR) -p $(@D)
	$(CC) -c $(CFLAGS_DONGLE) -Wa,-anlhd $< > $@		

-include $(DEPENDS_SONDE)
-include $(DEPENDS_SG04)
-include $(DEPENDS_DONGLE)

# dependencies file
# includes also considered, since some of these are our own
# (otherwise use -MM instead of -M)
%.d: %.c
	echo "Generating dependencies $@ from $<"
	$(CC) -M ${CFLAGS} $< >$@	
	
.SILENT:
.PHONY:	clean
clean:
	 rm -rf ./build_sg04/*
	 rm -rf ./build_sonde/*
	 rm -rf ./build_dongle/*


