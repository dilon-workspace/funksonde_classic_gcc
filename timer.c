//-----------------------------------------------------------------------
// timer.c
//
// Timer A0, A1 und A2
//  - Timer A0 wird f�r die interne Systemzeit benutzt
//  - Timer A1 und A2 werden f�r UDOut (Impulsemulation bei Empf�nger) bzw. Impulsaufnahme (Sender) verwendet
//
//-----------------------------------------------------------------------
//#include <msp430x26x.h>
#include <msp430.h>
#include "main_common.h"

#include <string.h>
#include <stdio.h>
// Externe Zeit
volatile uint16_t g_wMSTimer = 0;
volatile uint16_t g_wSecTimer = 0;
uint16_t g_wTicksTimer = 0;

uint16_t g_wBTicksToBeGeneratedA1 = 0;
uint16_t g_wBTicksToBeGeneratedA2 = 0;

// Tempor�re Vars
uint16_t g_wTimer = 0;
// uint16_t g_wBTicksToBeGenerated = 0;

// bool g_bBTicksTicking=false;

#define NOFIFO IS_PROBE

// void writeTicksTimer(t_tmrIntervalData *intervalData); // Ticks in den Buffer schreiben
// uint8_t loadTicksFromFifoToTimerA2(void);
// void tmrToggleOsziPin(void);
void doTickplayerTimeWindow(void);

// void tmrToggleOsziTasterPin(void);

void post_isr_timer_a0(void);

void _delay(uint32_t cntr) { // cntr: ticks � 7 x MCLK
    while (--cntr) {
        asm volatile("nop");
    };
}

void initTimerA0(void) {
    TA0R = 0;                               // Z�hlerregister initialisieren
    TA0CCR0 = FUNKSONDEN_CLOCK_Hz / 1000UL; // Vergleichswert
    // TA0CCR0 = 5000; // Vergleichswert
    TA0CCTL0 = CCIE;
    TA0CCTL1 = 0;
    TA0CCTL2 = 0;
    TA0CCTL3 = 0;
    TA0CCTL4 = 0;
    TA0CTL = TASSEL_2 + // SMCLK
             ID_0 +     // Teiler 1
             MC_1 +     // bis TA0CCR0 z�hlen
             TACLR +    // TAR autom. R�cksetzen
                        // TAIE +
             0;
}

void startTimerA1(void) { // Sondenbetrieb
    TA1R = 0;             // Z�hler r�cksetzen
    TA1CTL |= MC_1;       // Bis TA1CL0 z�hlen
}

void startTimerA2(void) { // Sondenbetrieb
    TA2R = 0;             // Z�hler r�cksetzen
    TA2CTL |= MC_1;       // Bis TA2CL0 z�hlen
}

void stoppTimerA1(void) {
    TA1CTL &= ~MC_3; // Timer stopped, mu� getrennt bleiben da Aufruf aus Interrupt erfolgt
}

void stoppTimerA2(void) {
    TA2CTL &= ~MC_3; // Timer stopped, mu� getrennt bleiben da Aufruf aus Interrupt erfolgt
}

//#pragma interrupt_handler intTimerB0:TIMERB0_VECTOR
__attribute__((interrupt(TIMER1_A0_VECTOR))) void isr_timer_a1(void) {
    uint16_t IntervalTotal;
    t_tmrIntervalData *intervalData;
    if (g_wBTicksToBeGeneratedA1)
        g_wBTicksToBeGeneratedA1--;
    if (g_wBTicksToBeGeneratedA1 == 0) {
        stoppTimerA1(); // Timer stopped
    }
}

__attribute__((interrupt(TIMER2_A0_VECTOR))) void isr_timer_a2(void) {
    uint16_t IntervalTotal;
    t_tmrIntervalData *intervalData;
    if (g_wBTicksToBeGeneratedA2)
        g_wBTicksToBeGeneratedA2--;
    if (g_wBTicksToBeGeneratedA2 == 0) {
        stoppTimerA2(); // Timer stopped
    }
}

void checkTimerA12(void) {
    uint16_t w = g_wMSTimer - g_wMSStartMeasure;
    if (w >= BTWINDOW) {
        TA1CTL &= ~MC_3;        // TimerA1 stopped
        g_wOldMeasureA1 = TA1R; // Z�hler auslesen
        // g_wOldMeasureA1 = 271;
        TA1R = 0;        // Z�hler r�cksetzen
        TA1CTL |= MC_1;  // Bis TBCL0 z�hlen
        TA2CTL &= ~MC_3; // TimerA1 stopped
                         // g_wOldMeasureA2 = TA2R;         // Z�hler auslesen
        // g_wOldMeasureA2 = 526;
        g_wOldMeasureA2 = TA2R;
        TA2R = 0;                       // Z�hler r�cksetzen
        TA2CTL |= MC_1;                 // Bis TBCL0 z�hlen
        g_wMSStartMeasure = g_wMSTimer; // Zeitfenster neu �ffnen
        g_ucMeasure = 1;                // Bekanntgabe, das neuer Wert vorliegt
        if (TA1CCTL0 & CCIFG) {         // Z�hler�berlauf abfangen
            g_wOldMeasureA1 = 0xFFFF;
            TA1CCTL0 &= ~CCIFG;
        }
        if (TA2CCTL0 & CCIFG) { // Z�hler�berlauf abfangen
            g_wOldMeasureA2 = 0xFFFF;
            TA2CCTL0 &= ~CCIFG;
        }
    }
}

void doTickplayerTimeWindow(void) {
}

//#pragma interrupt_handler intTimerA0:TIMERA0_VECTOR
__attribute__((interrupt(TIMER0_A0_VECTOR))) void isr_timer_a0(void) {
    static unsigned int ledtimer;
    // char ucBuffer[100];
    g_wMSTimer++;
    P2OUT ^= 0x02;
    if (g_ucOnOffDownfort == 1) // Berechnung erst nach registrierten Tastendruck
    {
        g_wOnOfffort = g_wMSTimer - g_wOnOffDownfort; // fortlaufende Berechnung der gedr�ckten Abschaltzeit
    }
    if (++g_wTimer > 999) {
        g_wTimer = 0;
        g_wSecTimer++;
        if (g_wBTRLast < 0xFFFF)
            g_wBTRLast++;

        if (g_wBTSLast < 0xFFFF)
            g_wBTSLast++;
    }
    // onLowBatLED();

    post_isr_timer_a0();
}
